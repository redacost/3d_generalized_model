"""
** generate **
generate showers using a saved VAE model
"""
import argparse

import numpy as np
import torch
import pickle

from core.constants import CONSTANTS
from utils.gpu_limiter import GPULimiter
from core.models.data import ShowersDataset
from utils.preprocess import get_condition_arrays, load_showers, preprocess_util, postprocess_util

GLOBAL_CHECKPOINT_DIR = CONSTANTS.GLOBAL_CHECKPOINT_DIR
GEN_DIR = CONSTANTS.GEN_DIR
BATCH_SIZE = CONSTANTS.BATCH_SIZE_PER_REPLICA
MAX_GPU_MEMORY_ALLOCATION = CONSTANTS.MAX_GPU_MEMORY_ALLOCATION
GPU_IDS = CONSTANTS.GPU_IDS
INIT_DIR = CONSTANTS.INIT_DIR
MODEL_TYPE = CONSTANTS.MODEL_TYPE


def parse_args():
    argument_parser = argparse.ArgumentParser()
    argument_parser.add_argument("--model-type", type=str, default=MODEL_TYPE)
    argument_parser.add_argument("--geometry", type=str, default="")
    argument_parser.add_argument("--energy", type=int, default="")
    argument_parser.add_argument("--angle", type=int, default="")
    argument_parser.add_argument("--events", type=int, default=10000)
    argument_parser.add_argument("--epoch", type=int, default=None)
    argument_parser.add_argument("--run-name", type=str, default="default_run_name")
    argument_parser.add_argument("--study-name", type=str, default="default_study_name")
    argument_parser.add_argument("--max-gpu-memory-allocation", type=int, default=MAX_GPU_MEMORY_ALLOCATION)
    argument_parser.add_argument("--gpu-ids", type=str, default=GPU_IDS)
    args = argument_parser.parse_args()
    return args


def generate(handler, dataloader, energy, device):
    generated_events = handler.predict(dataloader, device)
    generated_events = postprocess_util(generated_events, energy)
    return generated_events


# main function
def main():
    args = parse_args()
    model_type = args.model_type
    energy = args.energy
    angle = args.angle
    geometry = args.geometry
    events = args.events
    epoch = args.epoch
    run_name = args.run_name
    study_name = args.study_name
    max_gpu_memory_allocation = args.max_gpu_memory_allocation
    gpu_ids = args.gpu_ids

    # Set GPU memory limits.
    device = GPULimiter(_gpu_ids=gpu_ids, _max_gpu_memory_allocation=max_gpu_memory_allocation)()

    # Create a handler and build model.
    # This import must be local because otherwise it is impossible to call GPULimiter.
    from core.models import ResolveModel
    model_handler = ResolveModel(model_type)(_wandb_run_name=run_name, _wandb_project_name=study_name)

    # Load the saved weights
    model_handler.load_model()

    # Prediction
    model_handler._set_model_inference()
    showers = load_showers(INIT_DIR, geometry, energy, angle)
    energy_data, angle_data, geo_data = get_condition_arrays(geometry, energy, angle, showers.shape[0])
    data = [preprocess_util(showers, energy),] + [energy_data, angle_data, geo_data]
    dataset = ShowersDataset(data)
    dataloader = torch.utils.data.DataLoader(dataset, batch_size=BATCH_SIZE, num_workers=4)
    generated_events = generate(model_handler, dataloader, energy, device)

    # Save the generated showers.
    np.save(f"{GEN_DIR}/Geo_{geometry}_E_{energy}_Angle_{angle}.npy", generated_events)


if __name__ == "__main__":
    exit(main())

