u"""
Config file for experiments. Better if only native Python (and external) code is here, no internal imports,
to avoid cyclic import errors.

Edit: Set the variable as keys in dictionary so that they can be overwritten
by the cmd line easily.
"""

import torch

class AttributeDict(dict):
    __getattr__ = dict.__getitem__
    __setattr__ = dict.__setitem__


CONSTANTS = AttributeDict()

"""
Geometry constants.
"""
# Geometries to train on
CONSTANTS.GEOMETRIES = ["SiW",]  # ["SiW", "SciPb"]
# Number of calorimeter layers (z-axis segmentation).
CONSTANTS.ORG_N_CELLS_Z = 45#45
# Segmentation in the r,phi direction.
CONSTANTS.ORG_N_CELLS_R = 9#18
CONSTANTS.N_CELLS_PHI = 16#50
# Cell size in the r and z directions
CONSTANTS.SIZE_R = 2.325
CONSTANTS.SIZE_Z = 3.4

# In case of restricting data (Count from 0, Including)
CONSTANTS.R_HIGH = 8 #9 # None  # [0, 17]
CONSTANTS.Z_LOW = None #0 # None  # [0, 44]
CONSTANTS.Z_HIGH = None #23 # None  # [0, 44]
if CONSTANTS.R_HIGH is None: CONSTANTS.R_HIGH = 17
if CONSTANTS.Z_LOW is None: CONSTANTS.Z_LOW = 0
if CONSTANTS.Z_HIGH is None: CONSTANTS.Z_HIGH = 44
assert CONSTANTS.R_HIGH < CONSTANTS.ORG_N_CELLS_R
assert (CONSTANTS.Z_HIGH - CONSTANTS.Z_LOW) < CONSTANTS.ORG_N_CELLS_Z
CONSTANTS.N_CELLS_R = CONSTANTS.R_HIGH + 1
CONSTANTS.N_CELLS_Z = CONSTANTS.Z_HIGH - CONSTANTS.Z_LOW + 1

# Minimum and maximum primary particle energy to consider for training in GeV units.
CONSTANTS.ENERGIES = [64,] #[64, 128, 256]
CONSTANTS.MIN_ENERGY = min(CONSTANTS.ENERGIES)
CONSTANTS.MAX_ENERGY = max(CONSTANTS.ENERGIES)
# Minimum and maximum primary particle angle to consider for training in degrees units.
CONSTANTS.ANGLES = [70,]
CONSTANTS.MIN_ANGLE = min(CONSTANTS.ANGLES)
CONSTANTS.MAX_ANGLE = max(CONSTANTS.ANGLES)


"""
Directories.
"""
# Directory to save VAE checkpoints
CONSTANTS.GLOBAL_CHECKPOINT_DIR = "./checkpoint"
# Directory to save model after conversion to a format that can be used in C++.
CONSTANTS.CONV_DIR = "./conversion"
# Directory to save validation plots.
CONSTANTS.VALID_DIR = "./validation"
# Directory to save VAE generated showers.
CONSTANTS.GEN_DIR = "./generation"


"""
Model default parameters.
"""
CONSTANTS.MODEL_TYPE = 'DATASET2'#'DDPM_3D'#'VAE'
CONSTANTS.BATCH_SIZE_PER_REPLICA = 32
# Total number of readout cells (represents the number of nodes in the input/output layers of the model).
CONSTANTS.ORIGINAL_DIM = CONSTANTS.N_CELLS_Z * CONSTANTS.N_CELLS_R * CONSTANTS.N_CELLS_PHI
CONSTANTS.EPOCHS = 250
CONSTANTS.LEARNING_RATE = 0.001
CONSTANTS.VALIDATION_SPLIT = 0.10
CONSTANTS.OPTIMIZER_TYPE = torch.optim.Adam
CONSTANTS.SAVE_MODEL = True


"""
Managing GPUs
"""
# GPU identifiers separated by comma, no spaces.
CONSTANTS.GPU_IDS = "0,1,2,3" #"2,3"
# Maximum allowed memory on one of the GPUs (in GB)
CONSTANTS.MAX_GPU_MEMORY_ALLOCATION = 31


"""
VAE params
"""
CONSTANTS.INTERMEDIATE_DIMS = [2048, 1024, 512, 512, 512, 512] #[100, 50, 20, 14]
CONSTANTS.LATENT_DIM = 1024  # Also applicable to TransformerVAE
CONSTANTS.ACTIVATION = torch.nn.LeakyReLU # try with torch.nn.Tanh
CONSTANTS.KL_WEIGHT = 1
CONSTANTS.OUT_ACTIVATION = torch.nn.Sigmoid
CONSTANTS.KERNEL_INITIALIZER = "RandomNormal"
CONSTANTS.BIAS_INITIALIZER = "Zeros"


"""
Transformer parameters.
"""
CONSTANTS.NUM_LAYERS = 4
CONSTANTS.NUM_HEADS = 8
CONSTANTS.QKV_DIM = 128
CONSTANTS.PROJECTION_DIM = 128
CONSTANTS.FF_DIM = 128
CONSTANTS.MASKING_PERCENT = 0.75
CONSTANTS.DROPOUT = 0.1
CONSTANTS.PATCH_R = 1
CONSTANTS.PATCH_PHI = 10
CONSTANTS.PATCH_Z = 15


"""
Optimizer parameters.
"""
CONSTANTS.N_TRIALS = 50
# Maximum size of a hidden layer
CONSTANTS.MAX_HIDDEN_LAYER_DIM = 2000


"""
Validator parameter.
"""
CONSTANTS.FULL_SIM_HISTOGRAM_COLOR = "blue"
CONSTANTS.ML_SIM_HISTOGRAM_COLOR = "red"
CONSTANTS.FULL_SIM_GAUSSIAN_COLOR = "green"
CONSTANTS.ML_SIM_GAUSSIAN_COLOR = "orange"
CONSTANTS.HISTOGRAM_TYPE = "step"


"""
W&B parameters.
"""
# Change this to your entity name.
CONSTANTS.WANDB_ENTITY = "redacost"
CONSTANTS.PLOT_FREQ = 25
CONSTANTS.PLOT_CONFIG = [
    [70, 64, 'SiW'],]
#     [70, 128, 'SiW'],
#     [70, 256, 'SiW'],
# ]  # List of [angle, energy, geometry]


"""
Experimental
"""
# More data
CONSTANTS.USE_MORE_DATA = False
# Directory to load the full simulation dataset.
if CONSTANTS.USE_MORE_DATA:
    CONSTANTS.INIT_DIR = "../../../../eos/geant4/fastSim/Par04/HighStat/detector_SiW/"
else:
    #CONSTANTS.INIT_DIR = "../../../../eos/geant4/fastSim/Par04_public/HDF5_Zenodo/"
    CONSTANTS.INIT_DIR = "/eos/user/r/redacost/FMFastSim/Dataset2/"
    #CONSTANTS.INIT_DIR = "/home/datascience/"

# Losses on shower observables
CONSTANTS.INLCUDE_PHYSICS_LOSS = False


"""
Transformer VAE
"""
CONSTANTS.SINKHORN_NUM_ITERATES = 100
CONSTANTS.TENSORBOARD = True
CONSTANTS.USE_KL = True
CONSTANTS.USE_NOISE = False
CONSTANTS.SINKHORN_WEIGHT = 5


"""
Dataset
"""
CONSTANTS.DATASET_NUMBER = 2
