#Pytorch data parallel
#
#https://pytorch.org/tutorials/beginner/blitz/data_parallel_tutorial.html

import plotly.graph_objects as go
from core.models.data import getShowersDataloaders
from core.constants import CONSTANTS

import torch
import torch.nn as nn
import torch.nn.functional as F

import torchvision
import torchvision.datasets as datasets

from core.models.handler import ModelHandler
import math

from utils.gpu_limiter import GPULimiter

from torchvision.transforms import ToTensor 

import matplotlib.pyplot as plt

from tqdm import tqdm

import os
import torchvision.transforms as T


N_CELLS_R = CONSTANTS.N_CELLS_R
N_CELLS_PHI = CONSTANTS.N_CELLS_PHI
N_CELLS_Z = CONSTANTS.N_CELLS_Z

ORIGINAL_DIM = CONSTANTS.ORIGINAL_DIM
LATENT_DIM = CONSTANTS.LATENT_DIM
ACTIVATION = CONSTANTS.ACTIVATION
OUT_ACTIVATION = CONSTANTS.OUT_ACTIVATION
KERNEL_INITIALIZER = CONSTANTS.KERNEL_INITIALIZER
BIAS_INITIALIZER = CONSTANTS.BIAS_INITIALIZER
INTERMEDIATE_DIMS = CONSTANTS.INTERMEDIATE_DIMS
KL_WEIGHT = CONSTANTS.KL_WEIGHT
SINKHORN_WEIGHT = CONSTANTS.SINKHORN_WEIGHT

GPU_IDS = CONSTANTS.GPU_IDS
MAX_GPU_MEMORY_ALLOCATION = CONSTANTS.MAX_GPU_MEMORY_ALLOCATION
GLOBAL_CHECKPOINT_DIR = CONSTANTS.GLOBAL_CHECKPOINT_DIR
MODEL_TYPE = CONSTANTS.MODEL_TYPE

EPOCHS = CONSTANTS.EPOCHS
LEARNING_RATE = CONSTANTS.LEARNING_RATE
OPTIMIZER_TYPE = CONSTANTS.OPTIMIZER_TYPE
SAVE_MODEL = CONSTANTS.SAVE_MODEL

BATCH_SIZE_PER_REPLICA = CONSTANTS.BATCH_SIZE_PER_REPLICA

class Diffusion:
    def __init__(self, noise_steps=1000, beta_start=1e-4, beta_end=0.02, img_size=256, device="cuda"):
        self.noise_steps = noise_steps
        self.beta_start = beta_start
        self.beta_end = beta_end
        self.img_size = img_size #size of the output
        self.device = device

        self.beta = self.prepare_noise_schedule().to(device)
        self.alpha = 1. - self.beta
        self.alpha_hat = torch.cumprod(self.alpha, dim=0)

        self.alphas_bar_sqrt = torch.sqrt(self.alpha_hat)
        self.one_minus_alphas_bar_sqrt = torch.sqrt(1 - self.alpha_hat)

    def prepare_noise_schedule(self):
        return torch.linspace(self.beta_start, self.beta_end, self.noise_steps)

    def extract(self, input, t, x):
        # extrac value in the x shape
        shape = x.shape
        out = torch.gather(input, 0, t.to(input.device))
        reshape = [t.shape[0]] + [1] * (len(shape) - 1)
        return out.reshape(*reshape)

    def noise_images(self, x, t, noise=None):
        if noise is None:
            noise = torch.randn_like(x)
        alphas_t = self.extract(self.alphas_bar_sqrt, t, x)
        alphas_1_m_t = self.extract(self.one_minus_alphas_bar_sqrt, t, x)
        return (alphas_t * x + alphas_1_m_t * noise), noise

    def sample_timesteps(self, n):
        return torch.randint(low=1, high=self.noise_steps, size=(n,))

    def sample(self, model, n):
        #logging.info(f"Sampling {n} new images....")
        #model.eval()
        with torch.no_grad():
            x = torch.randn((n, 1, self.img_size, self.img_size)).to(self.device) #create random latent
            # x = torch.randn((n, self.img_size)).to(self.device) #create random latent
            for i in reversed(range(1, self.noise_steps)):
                t = (torch.ones(n) * i).long().to(self.device)
                predicted_noise = model(x, t)
                alpha = self.extract(self.alpha, t, x) 
                alpha_hat = self.extract(self.alpha_hat, t, x) 
                beta = self.extract(self.beta, t, x) 
                if i > 1:
                    noise = torch.randn_like(x)
                else:
                    noise = torch.zeros_like(x)
                x = 1 / torch.sqrt(alpha) * (x - ((1 - alpha) / (torch.sqrt(1 - alpha_hat))) * predicted_noise) + torch.sqrt(beta) * noise
        #model.train()
        #might need preprocessing
        import pdb; pdb.pdb.set_trace()
        x = (x.clamp(-1, 1) + 1) / 2 #clamp between 0 and 1
        # x = (x * 255).type(torch.uint8)
        return x

#--------------------------------------------------------------------------------------------------------------------------------------------

class UNet(nn.Module):
    def __init__(self, c_in=3, c_out=3, time_dim=256, device="cuda"):
        super().__init__()
        self.device = device
        self.time_dim = time_dim
        self.inc = DoubleConv(c_in, 64)
        self.down1 = Down(64, 128)
        #self.sa1 = SelfAttention(128, 32)
        self.sa1 = SelfAttention(128, 16)
        self.down2 = Down(128, 256)
        self.sa2 = SelfAttention(256, 8)
        self.down3 = Down(256, 256)
        self.sa3 = SelfAttention(256, 4)

        self.bot1 = DoubleConv(256, 512)
        self.bot2 = DoubleConv(512, 512)
        self.bot3 = DoubleConv(512, 256)

        self.up1 = Up(512, 128)
        #self.sa4 = SelfAttention(128, 16)
        self.sa4 = SelfAttention(128, 8)
        self.up2 = Up(256, 64)
        self.sa5 = SelfAttention(64, 16)
        self.up3 = Up(128, 64)
        self.sa6 = SelfAttention(64, 32)
        self.outc = nn.Conv2d(64, c_out, kernel_size=1)

    def pos_encoding(self, t, channels):
        # time encoding
        # print(self.device)
        #inv_freq = 1.0 / ( 10000 ** (torch.arange(0, channels, 2, device=self.device).float() / channels))
        inv_freq = 1.0 / ( 10000 ** (torch.arange(0, channels, 2, device=t.device).float() / channels))
        #print(inv_freq.get_device())
        pos_enc_a = torch.sin(t.repeat(1, channels // 2) * inv_freq)
        pos_enc_b = torch.cos(t.repeat(1, channels // 2) * inv_freq)
        pos_enc = torch.cat([pos_enc_a, pos_enc_b], dim=-1)
        return pos_enc

    def forward(self, x, t):
        t = t.unsqueeze(-1).type(torch.float)
        t = self.pos_encoding(t, self.time_dim)

        x1 = self.inc(x)
        x2 = self.down1(x1, t)
        x2 = self.sa1(x2)
        x3 = self.down2(x2, t)
        x3 = self.sa2(x3)
        x4 = self.down3(x3, t)
        x4 = self.sa3(x4)

        x4 = self.bot1(x4)
        x4 = self.bot2(x4)
        x4 = self.bot3(x4)

        x = self.up1(x4, x3, t)
        x = self.sa4(x)
        x = self.up2(x, x2, t)
        x = self.sa5(x)
        x = self.up3(x, x1, t)
        x = self.sa6(x)
        output = self.outc(x)
        return output


class SelfAttention(nn.Module):
    def __init__(self, channels, size):
        super(SelfAttention, self).__init__()
        self.channels = channels
        self.size = size
        self.mha = nn.MultiheadAttention(channels, 4, batch_first=True)
        self.ln = nn.LayerNorm([channels])
        self.ff_self = nn.Sequential(
            nn.LayerNorm([channels]),
            nn.Linear(channels, channels),
            nn.GELU(),
            nn.Linear(channels, channels),
        )


    def forward(self, x):
        x = x.view(-1, self.channels, self.size * self.size).swapaxes(1, 2) # flattens
        x_ln = self.ln(x)
        attention_value, _ = self.mha(x_ln, x_ln, x_ln)
        attention_value = attention_value + x
        attention_value = self.ff_self(attention_value) + attention_value
        return attention_value.swapaxes(2, 1).view(-1, self.channels, self.size, self.size) # brings it back


class DoubleConv(nn.Module):
    def __init__(self, in_channels, out_channels, mid_channels=None, residual=False):
        super().__init__()
        self.residual = residual
        if not mid_channels:
            mid_channels = out_channels
        self.double_conv = nn.Sequential(
            nn.Conv2d(in_channels, mid_channels, kernel_size=3, padding=1, bias=False),
            nn.GroupNorm(1, mid_channels),
            nn.GELU(),
            nn.Conv2d(mid_channels, out_channels, kernel_size=3, padding=1, bias=False),
            nn.GroupNorm(1, out_channels),
        )

    def forward(self, x):
        if self.residual:
            return F.gelu(x + self.double_conv(x))
        else:
            return self.double_conv(x)


class Down(nn.Module):
    def __init__(self, in_channels, out_channels, emb_dim=256):
        super().__init__()
        self.maxpool_conv = nn.Sequential(
            nn.MaxPool2d(2),
            DoubleConv(in_channels, in_channels, residual=True),
            DoubleConv(in_channels, out_channels),
        )

        self.emb_layer = nn.Sequential(
            nn.SiLU(),
            nn.Linear(
                emb_dim,
                out_channels
            ),
        )

    def forward(self, x, t):
        x = self.maxpool_conv(x)
        emb = self.emb_layer(t)[:, :, None, None].repeat(1, 1, x.shape[-2], x.shape[-1])
        return x + emb


class Up(nn.Module):
    def __init__(self, in_channels, out_channels, emb_dim=256):
        super().__init__()

        self.up = nn.Upsample(scale_factor=2, mode="bilinear", align_corners=True)
        self.conv = nn.Sequential(
            DoubleConv(in_channels, in_channels, residual=True),
            DoubleConv(in_channels, out_channels, in_channels // 2),
        )

        self.emb_layer = nn.Sequential(
            nn.SiLU(),
            nn.Linear(
                emb_dim,
                out_channels
            ),
        )

    def forward(self, x, skip_x, t):
        x = self.up(x)
        x = torch.cat([skip_x, x], dim=1)
        x = self.conv(x)
        emb = self.emb_layer(t)[:, :, None, None].repeat(1, 1, x.shape[-2], x.shape[-1])
        return x + emb


#--------------------------------------------------------------------------------------------------------------------------------------------

class DDPMLoss:
    def __init__(self, model):
        self.model = model
        self._checkpoint_dir = GLOBAL_CHECKPOINT_DIR

    def __call__(self, y_pred, y_true):
        # y_pred = F.sigmoid(y_pred)
        # y_true = F.sigmoid(y_true)
        # loss = nn.BCELoss(reduction='sum')(y_pred, y_true)
        loss = nn.MSELoss(reduction='sum')(y_pred, y_true)
        return loss

class DDPMHandler(ModelHandler):
    def __init__(self, **kwargs):
        self.UNet = UNet(c_in=1, c_out=1)
        super().__init__(**kwargs)

    def __post_init__(self) -> None:
        self._run_name = '1'

        self._initialize_model()
        self._save_dir = f"{self._checkpoint_dir}/ddpm_results/{self._run_name}"

    def _get_wandb_extra_config(self):
        return {
            "activation": ACTIVATION,
            "out_activation": OUT_ACTIVATION,
            "intermediate_dims": INTERMEDIATE_DIMS,
            "latent_dim": LATENT_DIM,
            "num_layers": len(INTERMEDIATE_DIMS)
        }

    def _initialize_model(self):
        self._model = self.UNet
        self._loss = DDPMLoss(self._model)

    def _set_model_inference(self):
        self._decoder = self.UNet

    def _train_one_epoch(self, trainloader, validloader, optimizer, device, Diffusion):
        self._model.train()
        train_loss = 0.0
        for X, y in tqdm(trainloader):
            # for i in range(len(X)):
            #     X[i] = X[i].to(device) #image + all other information
            y = y.to(device) #just the same images as X
            X = X.to(device)

            t = self.Diffusion.sample_timesteps(X.shape[0]).to(device)
            #import pdb; pdb.pdb.set_trace()
            x_t, noise = self.Diffusion.noise_images(X, t) #no energy angle etc encoding

            optimizer.zero_grad()
            y_hat = self._model(x_t, t)
            loss = self._loss(y_hat, noise)
            loss.backward()
            optimizer.step()

            train_loss += loss.item()

        self._model.eval()
        val_loss = 0.0
        #sinkhorn_val_loss = 0.0
        with torch.no_grad():
            for X, y in validloader:
                # for i in range(len(X)):
                #     X[i] = X[i].to(device)
                y = y.to(device)
                X = X.to(device)

                t = self.Diffusion.sample_timesteps(X.shape[0]).to(device)
                x_t, noise = self.Diffusion.noise_images(X, t) #no energy angle etc encoding

                y_hat = self._model(x_t, t)
                loss = self._loss(y_hat, noise)

                val_loss += loss.item()
                #sinkhorn_val_loss += sinkhorn.item()

        return train_loss / len(trainloader), val_loss / len(validloader)

    def train_model(self, device):

        learning_rate = LEARNING_RATE
        epochs= EPOCHS
        optimizer_type = OPTIMIZER_TYPE
        checkpoint_dir = GLOBAL_CHECKPOINT_DIR
        save_model= SAVE_MODEL

        mnist_trainset = datasets.MNIST(root='./data', train=True, download=True, transform=torchvision.transforms.Compose([torchvision.transforms.Resize(32), torchvision.transforms.ToTensor()]))
        mnist_testset = datasets.MNIST(root='./data', train=False, download=True, transform=torchvision.transforms.Compose([torchvision.transforms.Resize(32), torchvision.transforms.ToTensor()]))

        trainloader = torch.utils.data.DataLoader(mnist_trainset, batch_size=BATCH_SIZE_PER_REPLICA * torch.cuda.device_count(), shuffle=False, num_workers=4)
        validloader = torch.utils.data.DataLoader(mnist_testset, batch_size=BATCH_SIZE_PER_REPLICA * torch.cuda.device_count(), shuffle=False, num_workers=4)

        #print(torch.cuda.device_count())
        if torch.cuda.device_count() > 1:
            print("Let's use", torch.cuda.device_count(), "GPUs!")
            # dim = 0 [30, xxx] -> [10, ...], [10, ...], [10, ...] on 3 GPUs
            self._model = nn.DataParallel(self._model)

        self._model.to(device)

        self.Diffusion = Diffusion(img_size=32, device=device)

        optimizer = optimizer_type(self._model.parameters(), learning_rate)

        scheduler = torch.optim.lr_scheduler.ReduceLROnPlateau(
            optimizer, 'min', factor=0.5, patience=30)


        val_loss_min = float('inf')
        for epoch in range(epochs):
            # One epoch
            #train_loss, val_loss, sinkhorn_val_loss = self._train_one_epoch(trainloader, validloader, optimizer, device)
            train_loss, val_loss = self._train_one_epoch(trainloader, validloader, optimizer, device, self.Diffusion)
            scheduler.step(val_loss)

            #print("Epoch {}:\tTrainLoss: {} \tValidLoss: {} \tSinkhornValidLoss: {}".format(epoch + 1, train_loss, val_loss, sinkhorn_val_loss))
            print("Epoch {}:\tTrainLoss: {} \tValidLoss: {} ".format(epoch + 1, train_loss, val_loss))


            # Save model if improvement
            if val_loss_min > val_loss:
                print("Saving model..")
                val_loss_min = val_loss
                if self._save_model:
                    self.save_model()

            if epoch % 50 == 0:
                self.show_results(validloader, device, epoch)

    def predict(self, dataloader, device):
        """
        Inference loop using self._decoder.
        """
        self._set_model_inference()
        self._decoder.to(device)
        self._decoder.eval()
        if not self.Diffusion:
            self.Diffusion = Diffusion(img_size=32, device=device)

        results = []
        i = 0
        for X, _ in dataloader:
            # for i in range(len(X)):
            #     X[i] = X[i].to(device)
            # X = X.to(device)
            test_images = X[0]
            test_images.to(device)
            
            with torch.no_grad():
                y_hat = self.Diffusion.sample(self._decoder, n=test_images.shape[0])
            results.append(y_hat)

            if i >= 5:
                break 
            i += 1


        results = torch.cat(results, dim=0)
        return results.detach().cpu().numpy()

    def save_model(self):
        os.makedirs(self._save_dir, exist_ok=True)
        if len(GPU_IDS) > 1:
            torch.save(self._model.module.state_dict(), f"{self._save_dir}/model_weights.pt")
        else:
            torch.save(self._model.state_dict(), f"{self._save_dir}/model_weights.pt")

    def load_model(self):
        self._model.load_state_dict(torch.load(f"{self._save_dir}/model_weights.pt"))
        self._model.eval()

    def show_results(self, data_loader, device, epoch):
        #import pdb; pdb.pdb.set_trace()
        #import pdb; pdb.pdb.set_trace()
        images = self.predict(data_loader, device)

        f, axarr = plt.subplots(len(images))

        image_i = 0
        for image in images:
            axarr[image_i].imshow(image[0])
            axarr[image_i].axis('off')
            image_i += 1

        plt.savefig(f"{self._save_dir}/results_{str(epoch)}.png")

        return


def show_noise_images():
    # 1. Set GPU memory limits.
    device = GPULimiter(_gpu_ids=GPU_IDS, _max_gpu_memory_allocation=MAX_GPU_MEMORY_ALLOCATION)()

    # change original dim
    Diffusion_1 = Diffusion(img_size=ORIGINAL_DIM, device=device)


    mnist_trainset = datasets.MNIST(root='./data', train=True, download=True, transform=torchvision.transforms.Compose([torchvision.transforms.Resize(32), torchvision.transforms.ToTensor()]))
    mnist_testset = datasets.MNIST(root='./data', train=False, download=True, transform=torchvision.transforms.Compose([torchvision.transforms.Resize(32), torchvision.transforms.ToTensor()]))

    data_loader = torch.utils.data.DataLoader(mnist_trainset,
                                          batch_size=16,
                                          shuffle=False,
                                          num_workers=4)

    for X, y in data_loader:
        images = X[0:5]
        #print(images.size())
        #images = images.view(-1,1,32*32)
        images.to(device)
        break

    #plt.imshow(image[0])#,  cmap='gray', vmin=0, vmax=1)
    #plt.savefig("dummy_name_1.png")

    f, axarr = plt.subplots(5,11)

    image_i = 0
    for image in images:
        #print(image)
        axarr[image_i,0].imshow(image[0])
        #axarr[image_i,0].plot(image[0])
        #axarr[image_i,0].imshow(image[0].view(32,32))
        axarr[image_i,0].axis('off')

        for t in range(100, 1100, 100):
            print(t)
            noised_image, noise = Diffusion_1.noise_images( image[0].to(device), torch.tensor([t-1], dtype=torch.int64).to(device), noise=None)
            axarr[image_i,t//100].imshow(noised_image.cpu())
            #axarr[image_i,t//100].plot(noised_image.cpu())
            #axarr[image_i,t//100].imshow(noised_image.cpu().view(32,32))
            axarr[image_i,t//100].axis('off')
        
        image_i += 1

    plt.savefig("dummy_name.png")

    return

def show_result_images():
    GPULimiter(_gpu_ids=GPU_IDS, _max_gpu_memory_allocation=MAX_GPU_MEMORY_ALLOCATION)()
    device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
    print(device)

    handler = DDPMHandler()

    mnist_testset = datasets.MNIST(root='./data', train=False, download=True, transform=torchvision.transforms.Compose([torchvision.transforms.Resize(32), torchvision.transforms.ToTensor()]))
    data_loader = torch.utils.data.DataLoader(mnist_testset, batch_size=16, shuffle=False, num_workers=4)

    handler.load_model()

    images = handler.predict(data_loader, device)

    f, axarr = plt.subplots(images.size()[0])

    image_i = 0
    for image in images:
        axarr[image_i] = image
        image_i += 1

    plt.savefig("dummy_name.png")

    return



def main():
    print("Starting")

    GPULimiter(_gpu_ids=GPU_IDS, _max_gpu_memory_allocation=MAX_GPU_MEMORY_ALLOCATION)()
    device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
    print(device)

    handler = DDPMHandler()

    handler.train_model(device)


if __name__ == "__main__":
    #main()
    show_result_images()