#-------------------------------------------------------------------------------------------------------------------
# check https://github.com/openai/guided-diffusion
# https://huggingface.co/blog/annotated-diffusion

#https://arxiv.org/pdf/2006.04768v3.pdf for linear self attetion layers?
#-------------------------------------------------------------------------------------------------------------------

import plotly.graph_objects as go
from core.models.data import getShowersDataloaders
from core.constants import CONSTANTS

import torch
import torch.nn as nn
import torch.nn.functional as F

from core.models.handler import ModelHandler
import math


N_CELLS_R = CONSTANTS.N_CELLS_R
N_CELLS_PHI = CONSTANTS.N_CELLS_PHI
N_CELLS_Z = CONSTANTS.N_CELLS_Z

ORIGINAL_DIM = CONSTANTS.ORIGINAL_DIM
LATENT_DIM = CONSTANTS.LATENT_DIM
ACTIVATION = CONSTANTS.ACTIVATION
OUT_ACTIVATION = CONSTANTS.OUT_ACTIVATION
KERNEL_INITIALIZER = CONSTANTS.KERNEL_INITIALIZER
BIAS_INITIALIZER = CONSTANTS.BIAS_INITIALIZER
INTERMEDIATE_DIMS = CONSTANTS.INTERMEDIATE_DIMS
KL_WEIGHT = CONSTANTS.KL_WEIGHT
SINKHORN_WEIGHT = CONSTANTS.SINKHORN_WEIGHT


class Diffusion:
    def __init__(self, noise_steps=1000, beta_start=1e-4, beta_end=0.02, img_size=256, device="cuda"):
        self.noise_steps = noise_steps
        self.beta_start = beta_start
        self.beta_end = beta_end
        self.img_size = img_size #size of the output
        self.device = device

        self.beta = self.prepare_noise_schedule().to(device)
        self.alpha = 1. - self.beta
        self.alpha_hat = torch.cumprod(self.alpha, dim=0)

        self.alphas_bar_sqrt = torch.sqrt(self.alpha_hat)
        self.one_minus_alphas_bar_sqrt = torch.sqrt(1 - self.alpha_hat)

    def prepare_noise_schedule(self):
        return torch.linspace(self.beta_start, self.beta_end, self.noise_steps)

    def extract(self, input, t, x):
        # extrac value in the x shape
        shape = x.shape
        out = torch.gather(input, 0, t.to(input.device))
        reshape = [t.shape[0]] + [1] * (len(shape) - 1)
        return out.reshape(*reshape)

    def noise_images(self, x, t, noise=None):
        if noise is None:
            noise = torch.randn_like(x)
        alphas_t = self.extract(self.alphas_bar_sqrt, t, x)
        alphas_1_m_t = self.extract(self.one_minus_alphas_bar_sqrt, t, x)
        return (alphas_t * x + alphas_1_m_t * noise), noise

    def sample_timesteps(self, n):
        return torch.randint(low=1, high=self.noise_steps, size=(n,))

    def sample(self, model, n):
        #logging.info(f"Sampling {n} new images....")
        #model.eval()
        with torch.no_grad():
            #x = torch.randn((n, 3, self.img_size, self.img_size)).to(self.device) #create random latent
            x = torch.randn((n, self.img_size)).to(self.device) #create random latent
            for i in reversed(range(1, self.noise_steps)):
                t = (torch.ones(n) * i).long().to(self.device)
                predicted_noise = model(x, t)
                alpha = self.extract(self.alpha, t, x) 
                alpha_hat = self.extract(self.alpha_hat, t, x) 
                beta = self.extract(self.beta, t, x) 
                if i > 1:
                    noise = torch.randn_like(x)
                else:
                    noise = torch.zeros_like(x)
                x = 1 / torch.sqrt(alpha) * (x - ((1 - alpha) / (torch.sqrt(1 - alpha_hat))) * predicted_noise) + torch.sqrt(beta) * noise
        #model.train()
        #might need preprocessing
        x = (x.clamp(-1, 1) + 1) / 2 #clamp between 0 and 1
        # x = (x * 255).type(torch.uint8)
        return x

#--------------------------------------------------------------------------------------------------------------------------------------------

#change into a linear based Unet    

class Linear_UNet(nn.Module):
    def __init__(self, original_dim, intermediate_dims, latent_dim, time_dim=256, device="cuda"):
        super().__init__()
        
        self.intermediate_dims = intermediate_dims
        self.original_dim = original_dim 
        self.latent_dim = latent_dim 

        self.device = device
        self.time_dim = time_dim
        self.inc = DoubleLinear(self.original_dim, self.intermediate_dims[0])
        self.down1 = DownLinear(self.intermediate_dims[0], self.intermediate_dims[1])
        #self.sa1 = SelfAttentionLinear(self.intermediate_dims[1], 32)
        self.down2 = DownLinear(self.intermediate_dims[1], self.intermediate_dims[2])
        #self.sa2 = SelfAttentionLinear(self.intermediate_dims[2], 16)
        self.down3 = DownLinear(self.intermediate_dims[2], self.intermediate_dims[3])
        #self.sa3 = SelfAttentionLinear(self.intermediate_dims[3], 8)

        self.bot1 = DoubleLinear(self.intermediate_dims[3], self.latent_dim)
        self.bot2 = DoubleLinear(self.latent_dim, self.latent_dim)
        self.bot3 = DoubleLinear(self.latent_dim, self.intermediate_dims[3])

        self.up1 = UpLinear(self.intermediate_dims[3], self.intermediate_dims[2])
        #self.sa4 = SelfAttentionLinear(self.intermediate_dims[2], 16)
        self.up2 = UpLinear(self.intermediate_dims[2], self.intermediate_dims[1])
        #self.sa5 = SelfAttentionLinear(self.intermediate_dims[1], 32)
        self.up3 = UpLinear(self.intermediate_dims[1], self.intermediate_dims[0])
        #self.sa6 = SelfAttentionLinear(self.intermediate_dims[0], 64)
        self.outc = nn.Linear(self.intermediate_dims[0], self.original_dim)
        #out activation
        #self.outa = out_activation

    # The SinusoidalPositionEmbeddings module takes a tensor of shape (batch_size, 1) as input (i.e. the noise levels of several noisy images in a batch), 
    # and turns this into a tensor of shape (batch_size, dim), with dim being the dimensionality of the position embeddings.
    def pos_encoding(self, t, channels):
        # time encoding
        # inv_freq = 1.0 / (
        #     10000
        #     ** (torch.arange(0, channels, 2, device=self.device).float() / channels)
        # )
        # pos_enc_a = torch.sin(t.repeat(1, channels // 2) * inv_freq)
        # pos_enc_b = torch.cos(t.repeat(1, channels // 2) * inv_freq)
        # pos_enc = torch.cat([pos_enc_a, pos_enc_b], dim=-1)
        # return pos_enc

        device = t.device
        half_dim = self.time_dim // 2
        embeddings = math.log(10000) / (half_dim - 1)
        embeddings = torch.exp(torch.arange(half_dim, device=device) * -embeddings)
        embeddings = t[:, None] * embeddings[None, :]
        embeddings = torch.cat((embeddings.sin(), embeddings.cos()), dim=-1)
        return embeddings

    def forward(self, x, t):
        t = t.type(torch.float) #t.unsqueeze(-1).type(torch.float) #no need to unsqueeze, using linear
        t = self.pos_encoding(t, self.time_dim)

        x1 = self.inc(x)
        x2 = self.down1(x1, t)
        # x2 = self.sa1(x2)
        #print(x2.size())
        x3 = self.down2(x2, t)
        #print(x3.size())
        # x3 = self.sa2(x3)
        x4 = self.down3(x3, t)
        # x4 = self.sa3(x4)

        x4 = self.bot1(x4)
        x4 = self.bot2(x4)
        x4 = self.bot3(x4)

        x = self.up1(x4, x3, t)
        # x = self.sa4(x)
        x = self.up2(x, x2, t)
        # x = self.sa5(x)
        x = self.up3(x, x1, t)
        # x = self.sa6(x)
        output = self.outc(x)
        #output_with_a = self.outa(output)
        return output#_with_a

class DoubleLinear(nn.Module):
    def __init__(self, in_channels, out_channels, mid_channels=None, residual=False):
        super().__init__()
        self.activation = ACTIVATION
        self.residual = residual
        if not mid_channels:
            mid_channels = out_channels
        self.double_linear = nn.Sequential(
            nn.Linear(in_channels, mid_channels, bias=False),
            #nn.BatchNorm1d(mid_channels),
            #self.activation(),
            nn.GroupNorm(1, mid_channels),
            nn.GELU(),
            nn.Linear(mid_channels, out_channels, bias=False),
            #nn.BatchNorm1d(out_channels),
            nn.GroupNorm(1, out_channels),
        )

    def forward(self, x):
        if self.residual:
            return F.gelu(x + self.double_linear(x))
        else:
            return self.double_linear(x)

class DownLinear(nn.Module):
    def __init__(self, in_channels, out_channels, emb_dim=256):
        super().__init__()
        self.maxpool_linear = nn.Sequential(
            nn.Linear(in_channels, out_channels, bias=False),
            DoubleLinear(out_channels, out_channels, residual=True),
            DoubleLinear(out_channels, out_channels),
        )

        self.emb_layer = nn.Sequential(
            nn.SiLU(),
            nn.Linear(
                emb_dim,
                out_channels
            ),
        )

    def forward(self, x, t):
        x = self.maxpool_linear(x)
        # check emb layer
        #time embedding originally rearanges to "b c -> b c 1 1" if this is due to image and conv than it it not necessary
        emb = self.emb_layer(t)#[:, :, None, None].repeat(1, 1, x.shape[-2], x.shape[-1])
        return x + emb

class SelfAttentionLinear(nn.Module):
    def __init__(self, channels, size):
        super(SelfAttentionLinear, self).__init__()
        self.channels = channels
        self.size = size
        self.mha = nn.MultiheadAttention(channels, 4, batch_first=True)
        self.ln = nn.LayerNorm([channels])
        self.ff_self = nn.Sequential(
            nn.LayerNorm([channels]),
            nn.Linear(channels, channels),
            nn.GELU(),
            nn.Linear(channels, channels),
        )
        self.output = nn.LayerNorm(channels * 4 * self.size, self.channels)


    def forward(self, x): #need to change inout and output reshape and also play with the number of heads
        # x = x.view(-1, self.channels, self.size * self.size).swapaxes(1, 2) # flattens
        x_ln = self.ln(x)#[:,None,:] torch.unsqueeze(x, dim=1)
        attention_value, _ = self.mha(x_ln, x_ln, x_ln)
        attention_value = attention_value + x
        attention_value = self.ff_self(attention_value) + attention_value
        #print(attention_value.size())
        #x_out = attention_value.view(-1, attention_value[1] * attention_value[2])
        #x_out = self.output(x_out)
        #return attention_value # .swapaxes(2, 1).view(-1, self.channels, self.size, self.size) # brings it back
        return attention_value #x_out

class UpLinear(nn.Module):
    def __init__(self, in_channels, out_channels, emb_dim=256):
        super().__init__()

        self.up = nn.Linear(in_channels, out_channels, bias=False)
        self.linear = nn.Sequential(
            DoubleLinear(out_channels * 2, out_channels * 2, residual=True),
            DoubleLinear(out_channels * 2, out_channels),
        )

        self.emb_layer = nn.Sequential(
            nn.SiLU(),
            nn.Linear(
                emb_dim,
                out_channels
            ),
        )

    def forward(self, x, skip_x, t):
        x = self.up(x)
        x = torch.cat([skip_x, x], dim=1)
        x = self.linear(x)
        emb = self.emb_layer(t)#[:, :, None, None].repeat(1, 1, x.shape[-2], x.shape[-1])
        return x + emb


#--------------------------------------------------------------------------------------------------------------------------------------------

class DDPMLoss:
    def __init__(self, model):
        self.model = model

    def __call__(self, y_pred, y_true):
        # y_pred = F.sigmoid(y_pred)
        # y_true = F.sigmoid(y_true)
        # loss = nn.BCELoss(reduction='sum')(y_pred, y_true)
        loss = nn.MSELoss(reduction='sum')(y_pred, y_true)
        return loss

class DDPMHandler(ModelHandler):
    def __init__(self, **kwargs):
        self._Linear_UNet = Linear_UNet(ORIGINAL_DIM, INTERMEDIATE_DIMS, LATENT_DIM)
        super().__init__(**kwargs)

    def _get_wandb_extra_config(self):
        return {
            "activation": ACTIVATION,
            "out_activation": OUT_ACTIVATION,
            "intermediate_dims": INTERMEDIATE_DIMS,
            "latent_dim": LATENT_DIM,
            "num_layers": len(INTERMEDIATE_DIMS)
        }

    def _initialize_model(self):
        self._model = self._Linear_UNet
        self._loss = DDPMLoss(self._model)

    def _set_model_inference(self):
        self._decoder = self._Linear_UNet

    def _train_one_epoch(self, trainloader, validloader, optimizer, device):
        self.Diffusion = Diffusion(img_size=ORIGINAL_DIM, device=device)
        self._model.train()
        train_loss = 0.0
        for X, y in trainloader:
            for i in range(len(X)):
                X[i] = X[i].to(device) #image + all other information
            y = y.to(device) #just the same images as X

            t = self.Diffusion.sample_timesteps(y.shape[0]).to(device)
            x_t, noise = self.Diffusion.noise_images(y, t) #no energy angle etc encoding

            optimizer.zero_grad()
            y_hat = self._model(x_t, t)
            loss = self._loss(y_hat, noise)
            loss.backward()
            optimizer.step()

            train_loss += loss.item()

        self._model.eval()
        val_loss = 0.0
        #sinkhorn_val_loss = 0.0
        with torch.no_grad():
            for X, y in validloader:
                for i in range(len(X)):
                    X[i] = X[i].to(device)
                y = y.to(device)

                t = self.Diffusion.sample_timesteps(y.shape[0]).to(device)
                x_t, noise = self.Diffusion.noise_images(y, t) #no energy angle etc encoding

                y_hat = self._model(x_t, t)
                loss = self._loss(y_hat, noise)

                val_loss += loss.item()
                #sinkhorn_val_loss += sinkhorn.item()

        return train_loss / len(trainloader), val_loss / len(validloader)

    def predict(self, dataloader, device):
        """
        Inference loop using self._decoder.
        """
        self._set_model_inference()
        self._decoder.to(device)
        self._decoder.eval()
        self.Diffusion = Diffusion(img_size=ORIGINAL_DIM, device=device)

        results = []
        for _, y in dataloader:
            # for i in range(len(X)):
            #     X[i] = X[i].to(device)
            y = y.to(device)
            with torch.no_grad():
                y_hat = self.Diffusion.sample(self._decoder, n=y.shape[0])
            results.append(y_hat)

        results = torch.cat(results, dim=0)
        return results.detach().cpu().numpy()

#--------------------------------------------------------------------------------------------------------------------------------------------

# def train(args):
#     setup_logging(args.run_name)
#     device = args.device
#     dataloader = get_data(args)
#     model = UNet().to(device)
#     optimizer = optim.AdamW(model.parameters(), lr=args.lr)
#     mse = nn.MSELoss()
#     diffusion = Diffusion(img_size=args.image_size, device=device)
#     logger = SummaryWriter(os.path.join("runs", args.run_name))
#     l = len(dataloader)

#     for epoch in range(args.epochs):
#         logging.info(f"Starting epoch {epoch}:")
#         pbar = tqdm(dataloader)
#         for i, (images, _) in enumerate(pbar):
#             images = images.to(device)
#             t = diffusion.sample_timesteps(images.shape[0]).to(device)
#             x_t, noise = diffusion.noise_images(images, t)
#             predicted_noise = model(x_t, t)
#             loss = mse(noise, predicted_noise)

#             optimizer.zero_grad()
#             loss.backward()
#             optimizer.step()

#             pbar.set_postfix(MSE=loss.item())
#             logger.add_scalar("MSE", loss.item(), global_step=epoch * l + i)

#         sampled_images = diffusion.sample(model, n=images.shape[0])
#         save_images(sampled_images, os.path.join("results", args.run_name, f"{epoch}.jpg"))
#         torch.save(model.state_dict(), os.path.join("models", args.run_name, f"ckpt.pt"))
