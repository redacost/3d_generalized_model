import os
from dataclasses import dataclass

import numpy as np
import torch
import torch.nn as nn
import wandb
import plotly.graph_objects as go

#import torchinfo

from core import models
from validate import validate
from generate import generate
from utils.preprocess import load_showers, get_condition_arrays, preprocess_util
from core.models.data import getShowersDataloaders
from core.constants import CONSTANTS
from core.models.data import ShowersDataset

from core.models.diffusion import Diffusion

from tqdm import tqdm


BATCH_SIZE = CONSTANTS.BATCH_SIZE_PER_REPLICA
EPOCHS = CONSTANTS.EPOCHS
LEARNING_RATE = CONSTANTS.LEARNING_RATE
OPTIMIZER_TYPE = CONSTANTS.OPTIMIZER_TYPE
GLOBAL_CHECKPOINT_DIR = CONSTANTS.GLOBAL_CHECKPOINT_DIR
SAVE_MODEL = CONSTANTS.SAVE_MODEL
WANDB_ENTITY = CONSTANTS.WANDB_ENTITY
INIT_DIR = CONSTANTS.INIT_DIR
VALID_DIR = CONSTANTS.VALID_DIR
VALIDATION_SPLIT = CONSTANTS.VALIDATION_SPLIT
PLOT_FREQ = CONSTANTS.PLOT_FREQ
PLOT_CONFIG = CONSTANTS.PLOT_CONFIG
N_CELLS_R = CONSTANTS.N_CELLS_R
N_CELLS_PHI = CONSTANTS.N_CELLS_PHI
N_CELLS_Z = CONSTANTS.N_CELLS_Z

ORIGINAL_DIM = CONSTANTS.ORIGINAL_DIM


def ResolveModel(model_type):
    if model_type=='VAE':
        return models.VAEHandler
    if model_type=='TransformerVAE':
        return models.TransformerVAE
    if model_type=='TransformerMLM':
        return models.TransformerMLMHandler
    if model_type=='DDPM':
        return models.DDPMHandler
    if model_type=='VAE_TO_DDPM':
        return models.VAE_DDPM_Handler
    if model_type=='DDPM_3D':
        return models.DDPMHandler_3D
    if model_type=='DATASET2':
        return models.DDPM_Dataset2_Handler
    else:
        raise ValueError


@dataclass
class ModelHandler:
    """
    Class to handle building and training of models.
    """
    _wandb_run_name: str = None
    _wandb_project_name: str = None
    _log_to_wandb: bool = False
    _batch_size: int = BATCH_SIZE
    _learning_rate: float = LEARNING_RATE
    _epochs: int = EPOCHS
    _optimizer_type = OPTIMIZER_TYPE
    _checkpoint_dir: str = GLOBAL_CHECKPOINT_DIR
    _save_model: bool = SAVE_MODEL
    _validation_split: float = VALIDATION_SPLIT

    _model = None
    _decoder = None
    _loss = None

    def __post_init__(self) -> None:
        # Setup Wandb.
        if self._log_to_wandb:
            self._setup_wandb()

        self._initialize_model()
        self._save_dir = f"{self._checkpoint_dir}/{self._wandb_project_name}/{self._wandb_run_name}"

    def _setup_wandb(self) -> None:
        config = {
            "learning_rate": self._learning_rate,
            "batch_size": self._batch_size,
            "epochs": self._epochs,
            "optimizer_type": str(self._optimizer_type)
        }
        # Add model specific config
        config.update(self._get_wandb_extra_config())
        # Reinit flag is needed for hyperparameter tuning. Whenever new training is started, new Wandb run should be created.
        wandb.init(name=self._wandb_run_name, project=self._wandb_project_name, entity=WANDB_ENTITY, reinit=True, config=config)
        # Upload constants.py
        wandb.save("./core/constants.py")

    def _get_wandb_extra_config(self):
        raise NotImplementedError

    def _initialize_model(self):
        """
        Builds a model. Defines self._model. Should be defined by the inherited class.
        """
        raise NotImplementedError

    def _set_model_inference(self):
        """
        Inference pipeline. Defines self._decoder. Should be defined by the inherited class.
        """
        raise NotImplementedError

    def _get_dataloaders(self, dataset, e_cond, angle_cond, geo_cond):
        """
        Returns dataloaders for training. Should be defined by the inherited class.
        """
        return getShowersDataloaders(dataset, e_cond, angle_cond, geo_cond)

    def _train_one_epoch(self, trainloader, validloader, optimizer, device):
        self._model.train()
        train_loss = 0.0
        for X, y in tqdm(trainloader):
            for i in range(len(X)):
                X[i] = X[i].to(device)
            y = y.to(device)

            optimizer.zero_grad()
            y_hat = self._model(X)
            loss = self._loss(y_hat, y)
            loss.backward()
            optimizer.step()

            train_loss += loss.item()

        self._model.eval()
        val_loss = 0.0
        #sinkhorn_val_loss = 0.0
        with torch.no_grad():
            for X, y in validloader:
                for i in range(len(X)):
                    X[i] = X[i].to(device)
                y = y.to(device)

                y_hat = self._model(X)
                loss = self._loss(y_hat, y)

                val_loss += loss.item()
                #sinkhorn_val_loss += sinkhorn.item()

        return train_loss / len(trainloader), val_loss / len(validloader)#, sinkhorn_val_loss / len(validloader)

    
    def train_model(self, dataset, energy, angle, geo, device):
        trainloader, validloader = self._get_dataloaders(dataset, energy, angle, geo)

        self._model.to(device)
        self._device = device

        #for name, param in self._model.named_parameters():
        #    if param.requires_grad:
        #        print(name, param.size())

        #print(sum(p.numel() for p in self._model.parameters() if p.requires_grad))

        if torch.cuda.device_count() > 1:
            print("Let's use", torch.cuda.device_count(), "GPUs!")
            # dim = 0 [30, xxx] -> [10, ...], [10, ...], [10, ...] on 3 GPUs
            self._model = nn.DataParallel(self._model)
        
        self.Diffusion = Diffusion(device=device)

        self.show_noise_images(trainloader, self.Diffusion, self._device)
        return

        optimizer = self._optimizer_type(self._model.parameters(), self._learning_rate)

        scheduler = torch.optim.lr_scheduler.ReduceLROnPlateau(
            optimizer, 'min', factor=0.5, patience=30)

        wandb.watch(self._model, log_freq=100, log='all')
        shower_observables_callbacks = []
        for _angle, _energy, _geo in PLOT_CONFIG:
            shower_observables_callbacks.append(
                ValidationPlotCallback(PLOT_FREQ, self, _angle, _energy, _geo, device)
            )

        val_loss_min = float('inf')
        for epoch in range(self._epochs):
            # One epoch
            #train_loss, val_loss, sinkhorn_val_loss = self._train_one_epoch(trainloader, validloader, optimizer, device)
            train_loss, val_loss = self._train_one_epoch(trainloader, validloader, optimizer, device, self.Diffusion)
            scheduler.step(val_loss)

            #print("Epoch {}:\tTrainLoss: {} \tValidLoss: {} \tSinkhornValidLoss: {}".format(epoch + 1, train_loss, val_loss, sinkhorn_val_loss))
            print("Epoch {}:\tTrainLoss: {} \tValidLoss: {} ".format(epoch + 1, train_loss, val_loss))

            # WandB logs
            wandb.log({
                'train_loss': train_loss,
                'val_loss': val_loss,
                'epoch': epoch#,
                #'Sinkhorn_val': sinkhorn_val_loss
            })

            # Save model if improvement
            if val_loss_min > val_loss:
                print("Saving model..")
                val_loss_min = val_loss
                if self._save_model:
                    self.save_model()

            # Shower observables
            for so_config in shower_observables_callbacks:
                so_config(epoch)

    def predict(self, dataloader, device):
        """
        Inference loop using self._decoder.
        """
        self._set_model_inference()
        self._decoder.to(device)
        self._decoder.eval()

        results = []
        for X, _ in dataloader:
            for i in range(len(X)):
                X[i] = X[i].to(device)
            with torch.no_grad():
                y_hat = self._decoder(X)
            results.append(y_hat)

        results = torch.cat(results, dim=0)
        return results.detach().cpu().numpy()

    def save_model(self):
        os.makedirs(self._save_dir, exist_ok=True)
        torch.save(self._model.state_dict(), f"{self._save_dir}/model_weights.pt")

    def load_model(self):
        self._model.load_state_dict(torch.load(f"{self._save_dir}/model_weights.pt"))

    def model_summary(self):
        print(self._model)
        torchinfo.summary(self._model.encoder, (ORIGINAL_DIM +4,), batch_dim = 1, col_names = ("input_size", "output_size", "num_params", "kernel_size", "mult_adds"), verbose = 0)
        #[(ORIGINAL_DIM,), (1,), (1,), (1,)]

    def show_noise_images(self, trainloader, Diffusion, device):
        for X, y in trainloader:
            #import pdb; pdb.pdb.set_trace()
            energies = 64#X[1][0]
            angle = X[2][0]
            geo = X[3][0]
            images = y[0]
            images = images.reshape(-1,1,N_CELLS_R, N_CELLS_PHI, N_CELLS_Z)
            images = images * 2 - 1
            break


        #f, axarr = plt.subplots(5,11)

        shower_2 = load_showers(INIT_DIR, 'SiW', str(64), str(70))
        shower_2 = shower_2[0].reshape(N_CELLS_R, N_CELLS_PHI, N_CELLS_Z)

        def trace_3d_shower(shower, energy, mode=0):
            #shower = shower.clamp(0, 1) 
            shower = (shower.clamp(-1,1) + 1) / 2
            if mode == 0:
                shower = shower * energy
            r, phi, z, inn = np.stack([x.ravel() for x in np.mgrid[:N_CELLS_R, :N_CELLS_PHI, :N_CELLS_Z]] + [shower.ravel(),], axis=1).T
            phi = phi / phi.max() * 2 * np.pi
            x = r * np.cos(phi)
            y = r * np.sin(phi)

            # print('----------------------------------------------------')
            # print('inn')
            # print('----------------------------------------------------')
            # print(*inn, sep='\t')

            normalize_intensity_by = 30  # knob for transparency
            trace = go.Scatter3d(
                x=x,
                y=y,
                z=z,
                mode='markers',
                marker_symbol='square',
                marker_color=[f"rgba(0,0,255,{i*100//normalize_intensity_by/100})" for i in inn],
            )
            return trace

        index_i = 0
        trace_list = []
        for image in images:
            #print(image[0])
            #print(image[0].shape)
            #print(energies[index_i])
            trace = trace_3d_shower(image[0], 64)
            trace_list.append(trace)
            #print(trace)
            for t in range(50, 450, 50):
                print(t)
                image = image.to(device)
                x_t, noise = self.Diffusion.noise_images(image, torch.tensor([t-1], dtype=torch.int64).to(device))
                x_t = x_t.cpu()
                trace = trace_3d_shower(x_t, 64)
                trace_list.append(trace)
            index_i += 1

        # trace = trace_3d_shower(shower_2, 64, mode = 1)
        # trace_list.append(trace)
        
        go.Figure(data=trace_list).write_html(f"combinned_3d_shower.html")
        return


        # image_i = 0
        # for image in images:
        #     #print(image)
        #     axarr[image_i,0].imshow(image[0])
        #     #axarr[image_i,0].plot(image[0])
        #     #axarr[image_i,0].imshow(image[0].view(32,32))
        #     axarr[image_i,0].axis('off')

        #     for t in range(100, 1100, 100):
        #         print(t)
        #         noised_image, noise = Diffusion.noise_images( image[0].to(device), torch.tensor([t-1], dtype=torch.int64).to(device), noise=None)
        #         axarr[image_i,t//100].imshow(noised_image.cpu())
        #         #axarr[image_i,t//100].plot(noised_image.cpu())
        #         #axarr[image_i,t//100].imshow(noised_image.cpu().view(32,32))
        #         axarr[image_i,t//100].axis('off')
            
        #     image_i += 1

        # plt.savefig("dummy_name.png")
        # go.Figure(data=[trace,original_shower]).write_html(f"{VALID_DIR}/combinned_3d_shower.html")

        return


class ValidationPlotCallback:
    def __init__(self, verbose, handler, angle, energy, geometry, device):
        self.val_angle = angle
        self.val_energy = energy
        self.val_geometry = geometry
        self.handler = handler
        self.verbose = verbose
        self.device = device
        self._setup()

    def _setup(self):
        self.showers = load_showers(INIT_DIR, self.val_geometry, self.val_energy, self.val_angle)
        energy, angle, geo = get_condition_arrays(self.val_geometry, self.val_energy, self.val_angle, self.showers.shape[0])
        data = [preprocess_util(self.showers, self.val_energy),] + [energy, angle, geo]
        dataset = ShowersDataset(data)
        self.dataloader = torch.utils.data.DataLoader(dataset, batch_size=BATCH_SIZE, num_workers=4)

    def __call__(self, epoch):
        if (epoch % self.verbose)==0:
            print('Plotting..')
            self.handler._set_model_inference()
            generated_events = generate(self.handler, self.dataloader, self.val_energy, self.device)
            validate(self.showers, generated_events, self.val_energy, self.val_angle, self.val_geometry)

            observable_names = ["LatProf", "LongProf", "PhiProf", "E_tot", "E_cell", "E_cell_non_log", "E_cell_non_log_xlog",
                "E_layer", "LatFirstMoment", "LatSecondMoment", "LongFirstMoment", "LongSecondMoment", "Radial_num_zeroes"]
            plot_names = [
                f"{VALID_DIR}/{metric}_Geo_{self.val_geometry}_E_{self.val_energy}_Angle_{self.val_angle}"
                for metric in observable_names
            ]
            for file in plot_names:
                wandb.log({file: wandb.Image(f"{file}.png")})

            # 3D shower
            shower = generated_events[0]#.reshape(N_CELLS_R, N_CELLS_PHI, N_CELLS_Z)
            r, phi, z, inn = np.stack([x.ravel() for x in np.mgrid[:N_CELLS_R, :N_CELLS_PHI, :N_CELLS_Z]] + [shower.ravel(),], axis=1).T
            phi = phi / phi.max() * 2 * np.pi
            x = r * np.cos(phi)
            y = r * np.sin(phi)

            normalize_intensity_by = 30  # knob for transparency
            trace = go.Scatter3d(
                x=x,
                y=y,
                z=z,
                mode='markers',
                marker_symbol='square',
                marker_color=[f"rgba(0,0,255,{i*100//normalize_intensity_by/100})" for i in inn],
            )
            # go.Figure(trace).write_html(f"{VALID_DIR}/3d_shower.html")
            # wandb.log({'shower_{}_{}'.format(self.val_angle, self.val_energy): wandb.Html(f"{VALID_DIR}/3d_shower.html")})

            # Real 3D shower
            shower = self.showers[0].reshape(N_CELLS_R, N_CELLS_PHI, N_CELLS_Z)
            r, phi, z, inn = np.stack([x.ravel() for x in np.mgrid[:N_CELLS_R, :N_CELLS_PHI, :N_CELLS_Z]] + [shower.ravel(),], axis=1).T
            phi = phi / phi.max() * 2 * np.pi
            x = r * np.cos(phi)
            y = r * np.sin(phi)

            # new_inn = []
            # for el in inn:
            #     if el < 0:
            #         new_inn.append(0)
            #     else:
            #         new_inn.append(el)

            normalize_intensity_by = 30  # knob for transparency
            original_shower = go.Scatter3d(
                x=x,
                y=y,
                z=z,
                mode='markers',
                marker_symbol='square',
                marker_color=[f"rgba(255,0,0,{i*100//normalize_intensity_by/100})" for i in inn],
            )
            # go.Figure(original_shower).write_html(f"{VALID_DIR}/real_3d_shower.html")
            # wandb.log({'real_shower': wandb.Html(f"{VALID_DIR}/real_3d_shower.html")})

            # Combinned 3D shower
            go.Figure(data=[trace,original_shower]).write_html(f"{VALID_DIR}/combinned_3d_shower.html")
            wandb.log({'combinned_shower_{}_{}'.format(self.val_angle, self.val_energy): wandb.Html(f"{VALID_DIR}/combinned_3d_shower.html")})



# def _manufacture_callbacks(self) -> List[Callback]:
#     """
#     Based on parameters set by the user, manufacture callbacks required for training.

#     Returns:
#         A list of `Callback` objects.

#     """
#     callbacks = []
#     # If the early stopping flag is on then stop the training when a monitored metric (validation) has stopped
#     # improving after (patience) number of epochs.
#     if self._early_stop:
#         callbacks.append(
#             EarlyStopping(monitor="val_loss",
#                             min_delta=self._min_delta,
#                             patience=self._patience,
#                             verbose=True,
#                             restore_best_weights=True))
#     # Save model after every epoch.
#     if self._save_model_every_epoch:
#         callbacks.append(ModelCheckpoint(filepath=f"{self._checkpoint_dir}/Epoch_{{epoch:03}}/model_weights",
#                                             monitor="val_loss",
#                                             verbose=True,
#                                             save_weights_only=True,
#                                             mode="min",
#                                             save_freq="epoch"))
#     if self._save_best_model:
#         callbacks.append(ModelCheckpoint(filepath=f"{self._checkpoint_dir}/Best/model_weights",
#                                             monitor="val_loss",
#                                             verbose=True,
#                                             save_best_only=True,
#                                             save_weights_only=True,
#                                             mode="min",
#                                             save_freq="epoch"))

#     if self._tensorboard_callback:
#         callbacks.append(tf.keras.callbacks.TensorBoard(log_dir="/data/redacost/renato_gitlab/ml4fastsim/tensorboar_logdir"))


#     # Pass metadata to wandb.
#     callbacks.append(WandbCallback(
#         monitor="val_loss", verbose=0, mode="min", save_model=False))

#     for angle, energy, geometry in PLOT_CONFIG:
#         callbacks.append(ValidationPlotCallback(
#             PLOT_FREQ, self, angle, energy, geometry
#         ))

#     return callbacks

# class ValidationPlotCallback(Callback):
#     def __init__(self, verbose, handler, angle, energy, geometry):
#         super().__init__()
#         self.val_angle = angle
#         self.val_energy = energy
#         self.val_geometry = geometry
#         self.latent_dim = getattr(handler, 'latent_dim', None)
#         self.val_dataset = load_showers(INIT_DIR, geometry, energy, angle)
#         self.num_events = self.val_dataset.shape[0]
#         self.handler = handler
#         self.verbose = verbose

#     def on_epoch_end(self, epoch, logs=None):
#         if (epoch % self.verbose)==0:
#             print('Plotting..')
#             generator = self.handler.get_decoder()
#             generated_events = generate(generator, self.val_energy, self.val_angle, \
#                 self.val_geometry, self.num_events, self.latent_dim, self.val_dataset)
#             validate(self.val_dataset, generated_events, self.val_energy, self.val_angle, self.val_geometry)

#             observable_names = ["LatProf", "LongProf", "E_tot", "E_cell"]
#             plot_names = [
#                 f"{VALID_DIR}/{metric}_Geo_{self.val_geometry}_E_{self.val_energy}_Angle_{self.val_angle}"
#                 for metric in observable_names
#             ]
#             for file in plot_names:
#                 wandb.log({file: wandb.Image(f"{file}.png")})

#             # 3D shower
#             shower = generated_events[0].reshape(N_CELLS_R, N_CELLS_PHI, N_CELLS_Z)
#             r, phi, z, inn = np.stack([x.ravel() for x in np.mgrid[:N_CELLS_R, :N_CELLS_PHI, :N_CELLS_Z]] + [shower.ravel(),], axis=1).T
#             phi = phi / phi.max() * 2 * np.pi
#             x = r * np.cos(phi)
#             y = r * np.sin(phi)

#             normalize_intensity_by = 30  # knob for transparency
#             generated_shower = go.Scatter3d(
#                 x=x,
#                 y=y,
#                 z=z,
#                 mode='markers',
#                 marker_symbol='square',
#                 marker_color=[f"rgba(0,0,255,{i*100//normalize_intensity_by/100})" for i in inn],
#             )
#             # go.Figure(generated_shower).write_html(f"{VALID_DIR}/3d_shower.html")
#             # wandb.log({'shower': wandb.Html(f"{VALID_DIR}/3d_shower.html")})

#             # Real 3D shower
#             shower = self.val_dataset[0].reshape(N_CELLS_R, N_CELLS_PHI, N_CELLS_Z)
#             r, phi, z, inn = np.stack([x.ravel() for x in np.mgrid[:N_CELLS_R, :N_CELLS_PHI, :N_CELLS_Z]] + [shower.ravel(),], axis=1).T
#             phi = phi / phi.max() * 2 * np.pi
#             x = r * np.cos(phi)
#             y = r * np.sin(phi)

#             normalize_intensity_by = 30  # knob for transparency
#             original_shower = go.Scatter3d(
#                 x=x,
#                 y=y,
#                 z=z,
#                 mode='markers',
#                 marker_symbol='square',
#                 marker_color=[f"rgba(255,0,0,{i*100//normalize_intensity_by/100})" for i in inn],
#             )
#             # go.Figure(original_shower).write_html(f"{VALID_DIR}/real_3d_shower.html")
#             # wandb.log({'real_shower': wandb.Html(f"{VALID_DIR}/real_3d_shower.html")})

#             # Combinned 3D shower
#             go.Figure(data=[generated_shower,original_shower]).write_html(f"{VALID_DIR}/combinned_3d_shower.html")
#             wandb.log({'combinned_shower': wandb.Html(f"{VALID_DIR}/combinned_3d_shower.html")})
