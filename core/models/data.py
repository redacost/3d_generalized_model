import numpy as np
import torch

from core.constants import CONSTANTS

BATCH_SIZE = CONSTANTS.BATCH_SIZE_PER_REPLICA
VALID_SIZE = CONSTANTS.VALIDATION_SPLIT
ENERGIES = CONSTANTS.ENERGIES
ANGLES = CONSTANTS.ANGLES


def getDataloaders(dataset, batch_size, valid_size, num_workers=4):
    num_train = len(dataset)
    indices = list(range(num_train))
    np.random.shuffle(indices)
    split = int(np.floor(valid_size * num_train))
    train_idx, valid_idx = indices[split:], indices[:split]

    train_sampler = torch.utils.data.sampler.SubsetRandomSampler(train_idx)
    valid_sampler = torch.utils.data.sampler.SubsetRandomSampler(valid_idx)

    train_loader = torch.utils.data.DataLoader(dataset, batch_size=batch_size,
        sampler=train_sampler, num_workers=num_workers)
    valid_loader = torch.utils.data.DataLoader(dataset, batch_size=batch_size, 
        sampler=valid_sampler, num_workers=num_workers)

    return train_loader, valid_loader


class ShowersDataset(torch.utils.data.Dataset):
    """
    A custom PyTorch dataset class. Uses arrays, hence data needs to be in the RAM.
    X = Showers (+ Conditions)
    y = Showers
    """
    def __init__(self, data_tensors):
        self.data = data_tensors  # tuple of events, conditions, etc.
        self.num_events = len(self.data[0])
        self.num_conditions = len(data_tensors) - 1
        self._conversion()

    def __len__(self):
        return self.num_events

    def __getitem__(self, index):
        # Returns one sample
        X = []
        for array in self.data:
            X.append(torch.from_numpy(array[index]))

        y = X[0]
        return X, y

    def _conversion(self):
        self.data[0] = self.data[0].astype(np.float32)

        if len(self.data) > 2:
            # conditioning on angle and energy
            self.data[1] = self.data[1].astype(np.float32).reshape(-1, 1)
            self.data[2] = self.data[2].astype(np.float32).reshape(-1, 1)


class ShowersLabelsDataset(torch.utils.data.Dataset):
    """
    A custom PyTorch dataset class. Uses arrays, hence data needs to be in the RAM.
    X = Showers (+ Conditions)
    y = Labels
    """
    def __init__(self, data_tensors, label_array, label_mapping):
        self.data = data_tensors  # tuple of events, conditions, etc.
        self.num_events = len(self.data[0])
        self.num_conditions = len(data_tensors) - 1
        self.label_array = label_array
        self.label_mapping = label_mapping

    def __len__(self):
        return self.num_events

    def __getitem__(self, index):
        # Returns one sample
        X = []
        for array in self.data:
            X.append(torch.from_numpy(array[index]))

        y = self.label_mapping[str(self.label_array[index])]
        return X, y


def getShowersDataloaders(showers, energy=None, angle=None, geo=None):
    data = [showers,] + [i for i in [energy, angle, geo] if i is not None]
    dataset = ShowersDataset(data)
    return getDataloaders(dataset, BATCH_SIZE, VALID_SIZE)


def getShowersLabelsDataloaders(task, showers, energy=None, angle=None, geo=None):
    if task=='energy':
        assert energy
        label_array = energy
        labels = ENERGIES
    elif task=='angle':
        assert angle
        label_array = angle
        labels = ANGLES
    else:
        raise ValueError

    label_mapping = dict([(str(labels[i]), i) for i in range(len(labels))])

    data = [showers,] + [i for i in [energy, angle, geo] if i]
    dataset = ShowersLabelsDataset(data, label_array, label_mapping)
    return getDataloaders(dataset, BATCH_SIZE, VALID_SIZE)
