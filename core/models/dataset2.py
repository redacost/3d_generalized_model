import h5py
import pickle

#from core.models.data import getShowersDataloaders
from core.constants import CONSTANTS

import wandb
import plotly.graph_objects as go

import torch
import torch.nn as nn
import torch.nn.functional as F

import torchvision
import torchvision.datasets as datasets

from core.models.handler import ModelHandler, ValidationPlotCallback
import math

from utils.gpu_limiter import GPULimiter

from torchvision.transforms import ToTensor 

import matplotlib.pyplot as plt

from tqdm import tqdm

from core.models.diffusion import Diffusion

import os
import numpy as np

import json
from json import JSONEncoder


# N_CELLS_R = CONSTANTS.N_CELLS_R
# N_CELLS_PHI = CONSTANTS.N_CELLS_PHI
# N_CELLS_Z = CONSTANTS.N_CELLS_Z

LATENT_DIM = CONSTANTS.LATENT_DIM
ACTIVATION = CONSTANTS.ACTIVATION
OUT_ACTIVATION = CONSTANTS.OUT_ACTIVATION
KERNEL_INITIALIZER = CONSTANTS.KERNEL_INITIALIZER
BIAS_INITIALIZER = CONSTANTS.BIAS_INITIALIZER
INTERMEDIATE_DIMS = CONSTANTS.INTERMEDIATE_DIMS
KL_WEIGHT = CONSTANTS.KL_WEIGHT
SINKHORN_WEIGHT = CONSTANTS.SINKHORN_WEIGHT

GPU_IDS = CONSTANTS.GPU_IDS
MAX_GPU_MEMORY_ALLOCATION = CONSTANTS.MAX_GPU_MEMORY_ALLOCATION
GLOBAL_CHECKPOINT_DIR = CONSTANTS.GLOBAL_CHECKPOINT_DIR
MODEL_TYPE = CONSTANTS.MODEL_TYPE

EPOCHS = CONSTANTS.EPOCHS
LEARNING_RATE = CONSTANTS.LEARNING_RATE
OPTIMIZER_TYPE = CONSTANTS.OPTIMIZER_TYPE
SAVE_MODEL = CONSTANTS.SAVE_MODEL

BATCH_SIZE_PER_REPLICA = CONSTANTS.BATCH_SIZE_PER_REPLICA


INIT_DIR = CONSTANTS.INIT_DIR
ORIGINAL_DIM = CONSTANTS.ORIGINAL_DIM
MAX_ENERGY = CONSTANTS.MAX_ENERGY
MAX_ANGLE = CONSTANTS.MAX_ANGLE
GEOMETRIES = CONSTANTS.GEOMETRIES
R_HIGH = CONSTANTS.R_HIGH
Z_LOW = CONSTANTS.Z_LOW
Z_HIGH = CONSTANTS.Z_HIGH
ENERGIES = CONSTANTS.ENERGIES
ANGLES = CONSTANTS.ANGLES
USE_MORE_DATA = CONSTANTS.USE_MORE_DATA

BATCH_SIZE = CONSTANTS.BATCH_SIZE_PER_REPLICA

WANDB_ENTITY = CONSTANTS.WANDB_ENTITY
VALID_DIR = CONSTANTS.VALID_DIR
VALIDATION_SPLIT = CONSTANTS.VALIDATION_SPLIT
PLOT_FREQ = CONSTANTS.PLOT_FREQ
PLOT_CONFIG = CONSTANTS.PLOT_CONFIG

ORIGINAL_DIM = CONSTANTS.ORIGINAL_DIM

N_CELLS_R = 9
N_CELLS_PHI = 16
N_CELLS_Z = 45

def preprocess_2_train():
    """
    Preprocess for dataset 2 for the training values
    - only has energy and showers
    - energies from 1 GeV to 1 TeV
    """
    energies_train = []
    cond_e_train = []
    cond_angle_train = []
    cond_geo_train = []
    f_name = INIT_DIR + 'dataset_2_1.hdf5'
    h5 = h5py.File(f_name, "r")

    energies = h5['incident_energies'][:]
    showers = h5["showers"][:]

    #events = np.array(h5[f"{energy_particle}"])
    num_events = showers.shape[0]
    #events = preprocess_util(events, energy_particle) #check if it is correct

    # For restricted geometry
    #events = events[:, :(R_HIGH + 1), :, Z_LOW:(Z_HIGH + 1)]

    energies_train.append(showers.reshape(num_events, -1))

    angle_particle = 90

    # Bring the conditions b/w [0,1]
    max_e = 1000
    cond_e_train.append(energies / max_e)
    cond_angle_train.append([angle_particle / angle_particle] * num_events)
    # build the geometry condition vector (1 hot encoding vector)
    # if geo == "SiW":
    cond_geo_train.append([[0, 1]] * num_events)
    # if geo == "SciPb":
    #     cond_geo_train.append([[1, 0]] * num_events)

    # return numpy arrays
    energies_train = np.concatenate(energies_train)
    cond_e_train = np.concatenate(cond_e_train)
    cond_angle_train = np.concatenate(cond_angle_train)
    cond_geo_train = np.concatenate(cond_geo_train)
    return energies_train, cond_e_train, cond_angle_train, cond_geo_train


def preprocess_2_validation():
    """
    Preprocess for dataset 2 for the validation values
    - only has energy and showers
    - energies from 1 GeV to 1 TeV
    """
    energies_train = []
    cond_e_train = []
    cond_angle_train = []
    cond_geo_train = []
    f_name = INIT_DIR + 'dataset_2_2.hdf5'
    h5 = h5py.File(f_name, "r")

    energies = h5['incident_energies'][:]
    showers = h5["showers"][:]

    #events = np.array(h5[f"{energy_particle}"])
    num_events = showers.shape[0]
    #events = preprocess_util(events, energy_particle) #check if it is correct

    # For restricted geometry
    #events = events[:, :(R_HIGH + 1), :, Z_LOW:(Z_HIGH + 1)]

    energies_train.append(showers.reshape(num_events, -1))

    angle_particle = 90

    # Bring the conditions b/w [0,1]
    max_e = 1000
    cond_e_train.append(energies / max_e)
    cond_angle_train.append([angle_particle / angle_particle] * num_events)
    # build the geometry condition vector (1 hot encoding vector)
    # if geo == "SiW":
    cond_geo_train.append([[0, 1]] * num_events)
    # if geo == "SciPb":
    #     cond_geo_train.append([[1, 0]] * num_events)

    # return numpy arrays
    energies_train = np.concatenate(energies_train)
    cond_e_train = np.concatenate(cond_e_train)
    cond_angle_train = np.concatenate(cond_angle_train)
    cond_geo_train = np.concatenate(cond_geo_train)
    return energies_train, cond_e_train, cond_angle_train, cond_geo_train

def preprocess_2():
    energies_train, cond_e_train, cond_angle_train, cond_geo_train = preprocess_2_train()
    train = [energies_train, cond_e_train]#, cond_angle_train, cond_geo_train]
    energies_validation, cond_e_validation, cond_angle_validation, cond_geo_validation = preprocess_2_validation()
    validation = [energies_validation, cond_e_validation]#, cond_angle_validation, cond_geo_validation]
    return train, validation

class ShowersDataset(torch.utils.data.Dataset):
    """
    A custom PyTorch dataset class. Uses arrays, hence data needs to be in the RAM.
    X = Showers (+ Conditions)
    y = Showers
    """
    def __init__(self, data_tensors):
        self.data = data_tensors  # tuple of events, conditions, etc.
        self.num_events = len(self.data[0])
        self.num_conditions = len(data_tensors) - 1
        self._conversion()

    def __len__(self):
        return self.num_events

    def __getitem__(self, index):
        # Returns one sample
        X = []
        for array in self.data:
            X.append(torch.from_numpy(array[index]))

        y = X[0]
        return X, y

    def _conversion(self):
        self.data[0] = self.data[0].astype(np.float32)

        if len(self.data) > 2:
            # conditioning on angle and energy
            self.data[1] = self.data[1].astype(np.float32).reshape(-1, 1)
            self.data[2] = self.data[2].astype(np.float32).reshape(-1, 1)

def getDataloaders(traindataset, validationdataset, batch_size, num_workers=4):
    train_loader = torch.utils.data.DataLoader(traindataset, batch_size=batch_size, num_workers=num_workers)
    valid_loader = torch.utils.data.DataLoader(validationdataset, batch_size=batch_size, num_workers=num_workers)

    return train_loader, valid_loader

def getShowersDataloaders2(train, validation):
    traindataset = ShowersDataset(train)
    validationdataset = ShowersDataset(validation)
    return getDataloaders(traindataset, validationdataset, BATCH_SIZE)

def trace_3d_shower(shower, energy, mode=0):
        #shower = shower.clamp(0, 1) 
        shower = (shower.clamp(-1,1) + 1) / 2
        if mode == 0:
            shower = shower * energy
        r, phi, z, inn = np.stack([x.ravel() for x in np.mgrid[:N_CELLS_R, :N_CELLS_PHI, :N_CELLS_Z]] + [shower.ravel(),], axis=1).T
        phi = phi / phi.max() * 2 * np.pi
        x = r * np.cos(phi)
        y = r * np.sin(phi)

        # print('----------------------------------------------------')
        # print('inn')
        # print('----------------------------------------------------')
        # print(*inn, sep='\t')

        normalize_intensity_by = 30  # knob for transparency
        trace = go.Scatter3d(
            x=x,
            y=y,
            z=z,
            mode='markers',
            marker_symbol='square',
            marker_color=[f"rgba(0,0,255,{i*100//normalize_intensity_by/100})" for i in inn],
        )
        return trace

def trace_3d_shower(shower, energy, mode=0):
        #shower = shower.clamp(0, 1) 
        shower = (shower.clamp(-1,1) + 1) / 2
        if mode == 0:
            shower = shower * energy
        r, phi, z, inn = np.stack([x.ravel() for x in np.mgrid[:N_CELLS_R, :N_CELLS_PHI, :N_CELLS_Z]] + [shower.ravel(),], axis=1).T
        phi = phi / phi.max() * 2 * np.pi
        x = r * np.cos(phi)
        y = r * np.sin(phi)

        # print('----------------------------------------------------')
        # print('inn')
        # print('----------------------------------------------------')
        # print(*inn, sep='\t')

        normalize_intensity_by = 100  # knob for transparency
        trace = go.Scatter3d(
            x=x,
            y=y,
            z=z,
            mode='markers',
            marker_symbol='square',
            marker_color=[f"rgba(0,0,255,{i*100//normalize_intensity_by/100})" for i in inn],
        )
        return trace

def show_images():
    train, validation = preprocess_2()

    trainloader, validloader = getShowersDataloaders2(train, validation)

    for X, y in trainloader:
        #import pdb; pdb.pdb.set_trace()
        images = y[0]
        images = images.reshape(-1,1,N_CELLS_R, N_CELLS_PHI, N_CELLS_Z)
        images = images * 2 - 1
        break

    #print(images.shape)

    #return

    index_i = 0
    trace_list = []
    for image in images:
        #print(image[0])
        #print(image[0].shape)
        #print(energies[index_i])
        trace = trace_3d_shower(image[0], 64)
        trace_list.append(trace)

    go.Figure(data=trace_list).write_html(f"dataset2_3d_shower.html")
    return


def show_noise_images(device):
    train, validation = preprocess_2()

    trainloader, validloader = getShowersDataloaders2(train, validation)

    aDiffusion = Diffusion(device=device)

    for X, y in trainloader:
        #import pdb; pdb.pdb.set_trace()
        images = y[0]
        images = images.reshape(-1,1,N_CELLS_R, N_CELLS_PHI, N_CELLS_Z)
        images = images * 2 - 1
        break


    #f, axarr = plt.subplots(5,11)

    index_i = 0
    trace_list = []
    for image in images:
        #print(image[0])
        #print(image[0].shape)
        #print(energies[index_i])
        trace = trace_3d_shower(image[0], 64)
        trace_list.append(trace)
        #print(trace)
        for t in range(50, 450, 50):
            print(t)
            image = image.to(device)
            x_t, noise = aDiffusion.noise_images(image, torch.tensor([t-1], dtype=torch.int64).to(device))
            x_t = x_t.cpu()
            trace = trace_3d_shower(x_t, 64)
            trace_list.append(trace)
        index_i += 1

    # trace = trace_3d_shower(shower_2, 64, mode = 1)
    # trace_list.append(trace)
    
    go.Figure(data=trace_list).write_html(f"combinned_3d_shower.html")
    return

#--------------------------------------------------------------------------------------------------------------------------------------------

class UNet(nn.Module):
    def __init__(self, c_in=3, c_out=3, time_dim=256, device="cuda"):
        super().__init__()
        self.device = device
        self.time_dim = time_dim
        
        # ----------- (9,16,45)
        # self.pre_1 = nn.Upsample(size=(16,50,24))
        # self.pre_2 = nn.Conv3d(1,2,(1,10,5))
        # self.pre_3 = nn.Conv3d(2,4,(1,10,5))
        # self.pre_4 = nn.Conv3d(4,8,(1,9,1))
        # self.pre_5 = nn.Conv3d(8,8,(1,9,1))
        # c_in = 8
        # c_out = 8
        #------------
        
        self.inc = DoubleConv(c_in, 16)
        self.down1 = Down(16, 32, maxpooldim = (1,2,3))
        #self.sa1 = SelfAttention(128, 32)
        self.sa1 = SelfAttention(32, (9,8,15))
        self.down2 = Down(32, 64, maxpooldim = (1,2,3))
        self.sa2 = SelfAttention(64, (9,4,5))
        self.down3 = Down(64, 64, maxpooldim = (1,2,1))
        self.sa3 = SelfAttention(64, (9,2,5))

        self.bot1 = DoubleConv(64, 128)
        self.bot2 = DoubleConv(128, 128)
        self.bot3 = DoubleConv(128, 64)

        self.up1 = Up(128, 32, up_scale=(1,2,1))
        #self.sa4 = SelfAttention(128, 16)
        self.sa4 = SelfAttention(32, (9,4,5))
        self.up2 = Up(64, 16, up_scale=(1,2,3))
        self.sa5 = SelfAttention(16, (9,8,15))
        self.up3 = Up(32, 16, up_scale=(1,2,3))
        self.sa6 = SelfAttention(16, (9,16,45))
        self.outc = nn.Conv3d(16, c_out, kernel_size=1)
        
        #-------------
        # self.post_1 = nn.Upsample(scale_factor=(1,4,2))
        # self.post_2 = nn.Conv3d(8,4,kernel_size=3, padding=1)
        # self.post_3 = nn.Conv3d(4,2,kernel_size=(4,8,5))
        # self.post_4 = nn.Conv3d(2,1,kernel_size=(4,8,5))
        #-------------

    def pos_encoding(self, t, channels):
        # time encoding
        # print(self.device)
        #inv_freq = 1.0 / ( 10000 ** (torch.arange(0, channels, 2, device=self.device).float() / channels))
        inv_freq = 1.0 / ( 10000 ** (torch.arange(0, channels, 2, device=t.device).float() / channels))
        #print(inv_freq.get_device())
        pos_enc_a = torch.sin(t.repeat(1, channels // 2) * inv_freq)
        pos_enc_b = torch.cos(t.repeat(1, channels // 2) * inv_freq)
        pos_enc = torch.cat([pos_enc_a, pos_enc_b], dim=-1)
        return pos_enc

    def forward(self, imgs, inputs, t):
        #import pdb; pdb.pdb.set_trace()
        t = t.unsqueeze(-1).type(torch.float)
        t = self.pos_encoding(t, self.time_dim)
        
        #x, e_input, angle_input, geo_input = inputs
        #x, e_input = inputs
        x = imgs
        #inputs = [t, e_input, angle_input, geo_input]
        #inputs = [t, e_input]

        #t = torch.concat(inputs, dim=1)

        # x = self.pre_1(x)
        # x = self.pre_2(x)
        # x = self.pre_3(x)
        # x = self.pre_4(x)
        # x = self.pre_5(x)


        x1 = self.inc(x)
        x2 = self.down1(x1, t)
        x2 = self.sa1(x2)
        x3 = self.down2(x2, t)
        x3 = self.sa2(x3)
        x4 = self.down3(x3, t)
        x4 = self.sa3(x4)

        x4 = self.bot1(x4)
        x4 = self.bot2(x4)
        x4 = self.bot3(x4)

        x = self.up1(x4, x3, t)
        x = self.sa4(x)
        x = self.up2(x, x2, t)
        x = self.sa5(x)
        x = self.up3(x, x1, t)
        x = self.sa6(x)
        output = self.outc(x)

        
        # output = self.post_1(output)
        # output = self.post_2(output)
        # output = self.post_3(output)
        # post_output = self.post_4(output)
        
        return output


class SelfAttention(nn.Module): #change size?
    def __init__(self, channels, size):
        super(SelfAttention, self).__init__()
        self.channels = channels
        self.size = size
        self.mha = nn.MultiheadAttention(channels, 4, batch_first=True)
        self.ln = nn.LayerNorm([channels])
        self.ff_self = nn.Sequential(
            nn.LayerNorm([channels]),
            nn.Linear(channels, channels),
            nn.GELU(),
            nn.Linear(channels, channels),
        )


    def forward(self, x):
        x = x.view(-1, self.channels, self.size[0] * self.size[1] * self.size[2]).swapaxes(1, 2) # flattens
        x_ln = self.ln(x)
        attention_value, _ = self.mha(x_ln, x_ln, x_ln)
        attention_value = attention_value + x
        attention_value = self.ff_self(attention_value) + attention_value
        return attention_value.swapaxes(2, 1).view(-1, self.channels, self.size[0], self.size[1], self.size[2]) # brings it back


class DoubleConv(nn.Module):
    def __init__(self, in_channels, out_channels, mid_channels=None, residual=False):
        super().__init__()
        self.residual = residual
        if not mid_channels:
            mid_channels = out_channels
        # self.pad1 = (0,0,1,1,0,0)
        # self.pad2 = (1,1,0,0,1,1)
        self.double_conv = nn.Sequential(
            nn.Conv3d(in_channels, mid_channels, kernel_size=3, padding=1, bias=False),
            nn.GroupNorm(1, mid_channels),
            nn.GELU(),
            nn.Conv3d(mid_channels, out_channels, kernel_size=3, padding=1, bias=False),
            nn.GroupNorm(1, out_channels),
            #single conv
            # nn.Conv3d(in_channels, out_channels, kernel_size=3, padding=1, bias=False),
            #nn.Conv3d(in_channels, out_channels, kernel_size=3, bias=False),
            # nn.GroupNorm(1, out_channels),
            # nn.GELU(),
            
        )

    def forward(self, x):
        if self.residual:
            #import pdb; pdb.pdb.set_trace()
            # y = x
            # y = F.pad(x, self.pad1, 'circular')
            # y = F.pad(y, self.pad2 , 'constant', 0)
            # return F.gelu(x + self.double_conv(y))
            return F.gelu(x + self.double_conv(x))
        else:
            # x = F.pad(x, self.pad1, 'circular')
            # x = F.pad(x, self.pad2 , 'constant', 0)
            return self.double_conv(x)


class Down(nn.Module):
    def __init__(self, in_channels, out_channels, emb_dim=256, maxpooldim = 2):
        super().__init__()
        self.maxpool_conv = nn.Sequential(
            nn.MaxPool3d(maxpooldim),
            DoubleConv(in_channels, in_channels, residual=True),
            DoubleConv(in_channels, out_channels),
        )

        #emb_dim = emb_dim + 1

        self.emb_layer = nn.Sequential(
            nn.SiLU(),
            nn.Linear(
                emb_dim,
                out_channels
            ),
        )

    def forward(self, x, t):
        x = self.maxpool_conv(x)
        emb = self.emb_layer(t)[:, :, None, None, None].repeat(1, 1, x.shape[-3], x.shape[-2], x.shape[-1])
        return x + emb


class Up(nn.Module):
    def __init__(self, in_channels, out_channels, emb_dim=256, up_scale = 2):
        super().__init__()

        self.up = nn.Upsample(scale_factor=up_scale, mode="nearest")#, align_corners=True) #check mode
        self.conv = nn.Sequential(
            DoubleConv(in_channels, in_channels, residual=True),
            DoubleConv(in_channels, out_channels, in_channels // 2),
        )

        #emb_dim = emb_dim + 4

        self.emb_layer = nn.Sequential(
            nn.SiLU(),
            nn.Linear(
                emb_dim,
                out_channels
            ),
        )

    def forward(self, x, skip_x, t):
        x = self.up(x)
        x = torch.cat([skip_x, x], dim=1)
        x = self.conv(x)
        emb = self.emb_layer(t)[:, :, None, None, None].repeat(1, 1, x.shape[-3], x.shape[-2], x.shape[-1])
        return x + emb


#--------------------------------------------------------------------------------------------------------------------------------------------

class DDPMLoss:
    def __init__(self, model):
        self.model = model
        self._checkpoint_dir = GLOBAL_CHECKPOINT_DIR

    def __call__(self, y_pred, y_true):
        # y_pred = F.sigmoid(y_pred)
        # y_true = F.sigmoid(y_true)
        # loss = nn.BCELoss(reduction='sum')(y_pred, y_true)
        loss = nn.MSELoss(reduction='sum')(y_pred, y_true)
        return loss

class DDPM_Dataset2_Handler(ModelHandler):
    def __init__(self, **kwargs):
        self.UNet = UNet(c_in=1, c_out=1)
        super().__init__(**kwargs)

#     def __post_init__(self) -> None:
#         self._run_name = '1'

#         self._initialize_model()
#         self._save_dir = f"{self._checkpoint_dir}/ddpm_results/{self._run_name}"

    def _get_wandb_extra_config(self):
        return {
            "activation": ACTIVATION,
            "out_activation": OUT_ACTIVATION,
            "intermediate_dims": INTERMEDIATE_DIMS,
            "latent_dim": LATENT_DIM,
            "num_layers": len(INTERMEDIATE_DIMS)
        }

    def _initialize_model(self):
        self._model = self.UNet
        self._loss = DDPMLoss(self._model)

    def _set_model_inference(self):
        self._decoder = self.UNet

    def _train_one_epoch(self, trainloader, validloader, optimizer, device, Diffusion):
        self._model.train()
        train_loss = 0.0
        for X, y in tqdm(trainloader):
            for i in range(len(X)):
                X[i] = X[i].to(device) #image + all other information (energy)
            y = y.to(device).reshape(-1,1,N_CELLS_R, N_CELLS_PHI, N_CELLS_Z) #just the same images as X
            #X = X.to(device)

            
            t = self.Diffusion.sample_timesteps(y.shape[0]).to(device)
            #import pdb; pdb.pdb.set_trace()
            x_t, noise = self.Diffusion.noise_images(y, t) #no energy angle etc encoding

            optimizer.zero_grad()
            y_hat = self._model(x_t, X, t)
            loss = self._loss(y_hat, noise)
            loss.backward()
            optimizer.step()

            train_loss += loss.item()

        print('Added noise: ' + str(noise[0][0][0][0]) + ' | Predicted noise: ' + str(y_hat[0][0][0][0]))
        print('Added noise: ' + str(noise.shape) + ' | Predicted noise: ' + str(y_hat.shape))
        print(y.shape)

        class NumpyArrayEncoder(JSONEncoder):
            def default(self, obj):
                if isinstance(obj, numpy.ndarray):
                    return obj.tolist()
                return JSONEncoder.default(self, obj)

        self._model.eval()
        val_loss = 0.0
        #sinkhorn_val_loss = 0.0
        #import pdb; pdb.pdb.set_trace()
        with torch.no_grad():
            for X, y in tqdm(validloader):
                #import pdb; pdb.pdb.set_trace()
                for i in range(len(X)):
                    X[i] = X[i].to(device)
                y = y.to(device).reshape(-1,1,N_CELLS_R, N_CELLS_PHI, N_CELLS_Z)
                #X = X.to(device)
                #import pdb; pdb.pdb.set_trace()

                t = self.Diffusion.sample_timesteps(y.shape[0]).to(device)
                x_t, noise = self.Diffusion.noise_images(y, t) #no energy angle etc encoding
                y_hat = self._model(x_t, X, t)
                loss = self._loss(y_hat, noise)

                val_loss += loss.item()
                #sinkhorn_val_loss += sinkhorn.item()
            
            timesteps_list = []
            noise_list = []
            predicted_noise_list = []
            for t in tqdm(range(0, 400), desc='checking noise'):
                x_t, noise = self.Diffusion.noise_images(y, torch.tensor([t], dtype=torch.int64).to(device))
                y_hat = self._model(x_t, X, torch.tensor([t], dtype=torch.int64).to(device))
                timesteps_list.append(t)
                noise_list.append(noise.detach().cpu().numpy())
                predicted_noise_list.append(y_hat.detach().cpu().numpy())
            noise_pairs_dict = {'timestep' : timesteps_list, 'noise' : noise_list, 'predicted_noise' : predicted_noise_list}

            with open('noise_pairs_dict.json', 'w') as f:
                json.dump(noise_pairs_dict, f, cls=NumpyArrayEncoder)

        return train_loss / len(trainloader), val_loss / len(validloader)

    def train_model(self, train, validation, device):
        trainloader, validloader = getShowersDataloaders2(train, validation)

        self._model.to(device)
        self._device = device

        #for name, param in self._model.named_parameters():
        #    if param.requires_grad:
        #        print(name, param.size())

        #print(sum(p.numel() for p in self._model.parameters() if p.requires_grad))

        if torch.cuda.device_count() > 1:
            print("Let's use", torch.cuda.device_count(), "GPUs!")
            # dim = 0 [30, xxx] -> [10, ...], [10, ...], [10, ...] on 3 GPUs
            self._model = nn.DataParallel(self._model)
        
        self.Diffusion = Diffusion(device=device)

        # self.show_noise_images(trainloader, self.Diffusion, self._device)
        # return

        optimizer = self._optimizer_type(self._model.parameters(), self._learning_rate)

        scheduler = torch.optim.lr_scheduler.ReduceLROnPlateau(
            optimizer, 'min', factor=0.5, patience=30)

        wandb.watch(self._model, log_freq=100, log='all')
        shower_observables_callbacks = []
        for _angle, _energy, _geo in PLOT_CONFIG:
            shower_observables_callbacks.append(
                ValidationPlotCallback(PLOT_FREQ, self, _angle, _energy, _geo, device)
            )

        val_loss_min = float('inf')
        for epoch in range(self._epochs):
            # One epoch
            #train_loss, val_loss, sinkhorn_val_loss = self._train_one_epoch(trainloader, validloader, optimizer, device)
            train_loss, val_loss = self._train_one_epoch(trainloader, validloader, optimizer, device, self.Diffusion)
            scheduler.step(val_loss)

            #print("Epoch {}:\tTrainLoss: {} \tValidLoss: {} \tSinkhornValidLoss: {}".format(epoch + 1, train_loss, val_loss, sinkhorn_val_loss))
            print("Epoch {}:\tTrainLoss: {} \tValidLoss: {} ".format(epoch + 1, train_loss, val_loss))

            # WandB logs
            wandb.log({
                'train_loss': train_loss,
                'val_loss': val_loss,
                'epoch': epoch#,
                #'Sinkhorn_val': sinkhorn_val_loss
            })

            # Save model if improvement
            if val_loss_min > val_loss:
                print("Saving model..")
                val_loss_min = val_loss
                if self._save_model:
                    self.save_model()

            # Shower observables
            # for so_config in shower_observables_callbacks:
            #     so_config(epoch)

            print('Plotting')
            y_hat = self.predict(validloader, device)
            trace_3d_shower(y_hat, 64)



    def predict(self, dataloader, device):
        """
        Inference loop using self._decoder.
        """
        self._set_model_inference()
        self._decoder.to(device)
        self._decoder.eval()
        if not self.Diffusion:
            self.Diffusion = Diffusion(device=device)

        results = []
        j = 0
        for X, y in dataloader:
            print(j)
            for i in range(len(X)):
                X[i] = X[i].to(device)
            # X = X.to(device)
            y = y.to(device).reshape(-1,1,N_CELLS_R, N_CELLS_PHI, N_CELLS_Z)
            
            with torch.no_grad():
                y_hat = self.Diffusion.sample(self._decoder, n=y.shape[0], inputs=(y,X))
            #results.append(y_hat)

            if j >= 0:
                break 
            j += 1


        #results = torch.cat(results, dim=0)
        return y_hat.detach().cpu().numpy()

    def save_model(self):
        os.makedirs(self._save_dir, exist_ok=True)
        if len(GPU_IDS) > 1:
            torch.save(self._model.module.state_dict(), f"{self._save_dir}/model_weights.pt")
        else:
            torch.save(self._model.state_dict(), f"{self._save_dir}/model_weights.pt")

    def load_model(self):
        self._model.load_state_dict(torch.load(f"{self._save_dir}/model_weights.pt"))
        self._model.eval()

    def show_results(self, data_loader, device, epoch):
        #import pdb; pdb.pdb.set_trace()
        #import pdb; pdb.pdb.set_trace()
        images = self.predict(data_loader, device)

        f, axarr = plt.subplots(len(images))

        image_i = 0
        for image in images:
            axarr[image_i].imshow(image[0])
            axarr[image_i].axis('off')
            image_i += 1

        plt.savefig(f"{self._save_dir}/results_{str(epoch)}.png")

        return