import torch
import torch.nn as nn
import torch.nn.functional as F
import math
import numpy as np
from tqdm import tqdm

class Diffusion:
    def __init__(self, noise_steps=400, beta_start=1e-4, beta_end=0.02, img_size=(10,50,24), device="cuda"):
        self.noise_steps = noise_steps
        self.beta_start = beta_start
        self.beta_end = beta_end
        self.img_size = img_size #size of the output
        self.device = device

        self.beta = self.prepare_noise_schedule().to(device)
        self.alpha = 1. - self.beta
        self.alpha_hat = torch.cumprod(self.alpha, dim=0)
        self.alpha_hat_prev = F.pad(self.alpha_hat[:-1], (1, 0), value=1.0)
        self.sqrt_recip_alphas = torch.sqrt(1.0 / self.alpha)

        self.alphas_bar_sqrt = torch.sqrt(self.alpha_hat)
        #print(self.alphas_bar_sqrt.shape)
        self.one_minus_alphas_bar_sqrt = torch.sqrt(1 - self.alpha_hat)

        self.posterior_variance = self.beta * (1. - self.alpha_hat_prev) / (1. - self.alpha_hat)

    def prepare_noise_schedule(self):
        #return 
        a = torch.linspace(self.beta_start, self.beta_end, self.noise_steps)
        #print(a.shape)
        #elif schedule_name == "cosine":
        b = torch.tensor(self.betas_for_alpha_bar(
            self.noise_steps,
            lambda t: math.cos((t + 0.008) / 1.008 * math.pi / 2) ** 2,
        ))
        #print(b.shape)
        c = self.cosine_beta_schedule(self.noise_steps)
        #print(c.shape)
        return c

    def betas_for_alpha_bar(self, num_diffusion_timesteps, alpha_bar, max_beta=0.999):
        """
        Create a beta schedule that discretizes the given alpha_t_bar function,
        which defines the cumulative product of (1-beta) over time from t = [0,1].
        :param num_diffusion_timesteps: the number of betas to produce.
        :param alpha_bar: a lambda that takes an argument t from 0 to 1 and
                        produces the cumulative product of (1-beta) up to that
                        part of the diffusion process.
        :param max_beta: the maximum beta to use; use values lower than 1 to
                        prevent singularities.
        """
        betas = []
        for i in range(num_diffusion_timesteps):
            t1 = i / num_diffusion_timesteps
            t2 = (i + 1) / num_diffusion_timesteps
            betas.append(min(1 - alpha_bar(t2) / alpha_bar(t1), max_beta))
        return np.array(betas) 

    def cosine_beta_schedule(self, timesteps, s=0.008):
        """
        cosine schedule as proposed in https://arxiv.org/abs/2102.09672
        """
        steps = timesteps + 1
        x = torch.linspace(0, timesteps, steps)
        alphas_cumprod = torch.cos(((x / timesteps) + s) / (1 + s) * torch.pi * 0.5) ** 2
        alphas_cumprod = alphas_cumprod / alphas_cumprod[0]
        betas = 1 - (alphas_cumprod[1:] / alphas_cumprod[:-1])
        return torch.clip(betas, 0.0001, 0.9999)

    def extract(self, input, t, x):
        # extrac value in the x shape
        shape = x.shape
        out = torch.gather(input, -1, t.to(input.device))
        # reshape = [t.shape[0]] + [1] * (len(shape) - 1)
        # return out.reshape(*reshape)
        return out.reshape(t.shape[0], *((1,) * (len(shape) - 1))).to(input.device)

    def noise_images(self, x, t, noise=None):
        if noise is None:
            noise = torch.randn_like(x)
        alphas_t = self.extract(self.alphas_bar_sqrt, t, x)
        alphas_1_m_t = self.extract(self.one_minus_alphas_bar_sqrt, t, x)
        return (alphas_t * x + alphas_1_m_t * noise), noise

    def sample_timesteps(self, n):
        return torch.randint(low=0, high=self.noise_steps, size=(n,))

    def sample(self, model, n, inputs):
        #logging.info(f"Sampling {n} new images....")
        #model.eval()
        y, X = inputs
        inputs = X

        with torch.no_grad():
            #x = torch.randn((n, 1, self.img_size[0], self.img_size[1], self.img_size[2])).to(self.device) #create random latent
            x = torch.randn(y.shape, device=self.device) #random latent
            # x = torch.randn((n, self.img_size)).to(self.device) #create random latent
            #for i in reversed(range(1, self.noise_steps)):
            for i in tqdm(reversed(range(0, self.noise_steps)), desc='sampling loop time step', total=self.noise_steps):
                #t = (torch.ones(n) * i).long().to(self.device)
                t = torch.full((n,), i, device=self.device, dtype=torch.long) # n values with value i for time
                predicted_noise = model(x, inputs, t)
                alpha = self.extract(self.alpha, t, x) 
                alpha_hat = self.extract(self.alpha_hat, t, x) 
                beta = self.extract(self.beta, t, x) 
                posterior_variance_t = self.extract(self.posterior_variance, t, x)
                if i >= 1:
                    noise = torch.randn_like(x)
                else:
                    noise = torch.zeros_like(x)
                x_1 = 1 / torch.sqrt(alpha) * (x - ((1 - alpha) / (torch.sqrt(1 - alpha_hat))) * predicted_noise)
                x_2 = torch.sqrt(posterior_variance_t) * noise
                x = x_1 + x_2
        #model.train()
        #might need preprocessing
        #import pdb; pdb.pdb.set_trace()
        #x = x.clamp(0, 1) #clamp between 0 and 1
        x = (x.clamp(-1,1) + 1) / 2
        # x = (x * 255).type(torch.uint8)
        return x