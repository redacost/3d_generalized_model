from argparse import ArgumentParser

from core.constants import CONSTANTS
from utils.gpu_limiter import GPULimiter
from utils.preprocess import preprocess

from core.models.dataset2 import preprocess_2

import torch

GPU_IDS = CONSTANTS.GPU_IDS
MAX_GPU_MEMORY_ALLOCATION = CONSTANTS.MAX_GPU_MEMORY_ALLOCATION
GLOBAL_CHECKPOINT_DIR = CONSTANTS.GLOBAL_CHECKPOINT_DIR
MODEL_TYPE = CONSTANTS.MODEL_TYPE
DATASET_NUMBER = CONSTANTS.DATASET_NUMBER


def parse_args():
    argument_parser = ArgumentParser()
    argument_parser.add_argument("--model-type", type=str, default=MODEL_TYPE)
    argument_parser.add_argument("--max-gpu-memory-allocation", type=int, default=MAX_GPU_MEMORY_ALLOCATION)
    argument_parser.add_argument("--gpu-ids", type=str, default=GPU_IDS)
    argument_parser.add_argument("--run-name", type=str, default=None)  # randomly chosen by wandb
    argument_parser.add_argument("--study-name", type=str, default="default_study_name")
    args = argument_parser.parse_args()
    return args


def main():
    # 0. Parse arguments.
    args = parse_args()
    model_type = args.model_type
    max_gpu_memory_allocation = args.max_gpu_memory_allocation
    gpu_ids = args.gpu_ids
    study_name = args.study_name
    run_name = args.run_name
    checkpoint_dir = f"{GLOBAL_CHECKPOINT_DIR}"

    # 1. Set GPU memory limits.
    device = GPULimiter(_gpu_ids=gpu_ids, _max_gpu_memory_allocation=max_gpu_memory_allocation)()

    device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
    # print(device)

    # from core.models.dataset2 import show_noise_images
    # show_noise_images(device)
    # return

    # 2. Data loading/preprocessing

    # The preprocess function reads the data and performs preprocessing and encoding for the values of energy,
    # angle and geometry
    if int(DATASET_NUMBER) == 1:
        energies_train, cond_e_train, cond_angle_train, cond_geo_train = preprocess()
    elif int(DATASET_NUMBER) == 2:
        train, validation = preprocess_2()


    # from core.models.ddpm import forward_diffusion
    # forward_diffusion(energies_train, cond_e_train, cond_angle_train, cond_geo_train, device)

    # return

    # 3. Manufacture model handler.
    # This import must be local because otherwise it is impossible to call GPULimiter.
    from core.models import ResolveModel
    model_handler = ResolveModel(model_type)(
        _wandb_run_name=run_name, _wandb_project_name=study_name, _log_to_wandb=True
        )

    # 4 show model summary
    # model_handler.model_summary()


    # 5. Train model.
    if DATASET_NUMBER == 1:
        model_handler.train_model(energies_train, cond_e_train, cond_angle_train, cond_geo_train, device)
    elif DATASET_NUMBER == 2:
        model_handler.train_model(train, validation, device)

    # Note : One history object can be used to plot the loss evaluation as function of the epochs. Remember that the
    # function returns a list of those objects. Each of them represents a different fold of cross validation.


if __name__ == "__main__":
    exit(main())
